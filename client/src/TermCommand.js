import Command from './Command.js';

export default class TermCommand extends Command {
    get name() {
        return 'term';
    }

    get usage() {
        return '<args>';
    }

    execute(args) {
        return this.term(args.join(' '));
    }

    /**
     * @param {string} str
     */
    term(str) {
        return new Promise((resolve, reject) => {
            this.client.sendToServer({type: 'term-input', content: str}, false);
            resolve();
        });
    }

}
