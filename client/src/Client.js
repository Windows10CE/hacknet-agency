import readline from 'readline';
import {Socket} from 'net';
import util from 'util';

import ConnectCommand from './ConnectCommand.js';

import ExitCommand from './ExitCommand.js';
import HelpCommand from './HelpCommand.js';
import SelectCommand from './SelectCommand.js';
import ReadyCommand from './ReadyCommand.js';
import TermCommand from './TermCommand.js';

/** @typedef {import('./Command.js').default} Command */
/** @typedef {import('readline').Interface} Interface */
/**
 * @callback dataListener
 * @param {Object} data
 */

const PACKET_SEPARATOR = '\0';
const COMMAND_PROMPT_TIP = '> ';

export default class Client {
    /** @type {Interface} */
    rl;

    /** @type {string} */
    username;

    /** @type {dataListener[]} */
    dataListeners;

    /** @type {string} */
    packetBuffer;

    /** @type {Object.<string, dataListener>} */
    packetListeners;

    /** @type {Object.<string, Command>} */
    commands;

    /** @type {boolean} */
    commandRunning = false;

    /** @type {Socket} */
    socket = null;

    constructor() {
        this.dataListeners = [];
        this.packetBuffer = '';
        this.packetListeners = {};
        this.commands = {};

        this.registerCommand(new HelpCommand(this));
        this.registerCommand(new ExitCommand(this));
        this.registerCommand(new ConnectCommand(this));
        this.registerCommand(new SelectCommand(this));
        this.registerCommand(new ReadyCommand(this));
        this.registerCommand(new TermCommand(this));

        this.registerPacketListener('ping', data => this.handlePingPacket(data));
        this.registerPacketListener('disconnected', data => this.handleDisconnectedPacket(data));

        this.registerPacketListener('connection-result', data => this.handleConnectionResult(data));
        this.registerPacketListener('auth-result', data => this.handleAuthResult(data));

        this.registerPacketListener('world-info', data => this.log(util.inspect(data, false, null, true)));
        this.registerPacketListener('clients-info', data => this.log(data));

        this.registerPacketListener('terminal-msg', data => this.log(data.message));
    }

    /**
     * @param {Command} command
     */
    registerCommand(command) {
        if (this.commands[command.name]) {
            throw 'This command already exists';
        }
        this.commands[command.name] = command;
    }

    /**
     * @param {string} packetType
     * @param {dataListener} dataListener
     */
    registerPacketListener(packetType, dataListener) {
        if (this.packetListeners[packetType]) {
            throw 'This packet listener is already registered';
        }
        this.packetListeners[packetType] = dataListener;
    }

    run() {
        this.log('Initializing');
        this.rl = readline.createInterface(process.stdin, process.stdout);
        this.promptLoop();
    }

    promptLoop() {
        this.prompt().then(command => {
            this.commandRunning = true;
            this.handleCommand(command.split(' ')).then(() => {
                this.commandRunning = false;
                this.promptLoop();
            });
        }).catch(err => {
            this.log(err, true);
            process.exit(-1);
        });
    }

    /**
     * @param {string} [message]
     * @return Promise<string>
     */
    prompt(message) {
        if (typeof message !== 'string') { message = ''; }

        return new Promise(resolve => {
            this.rl.question(message + '\n' + COMMAND_PROMPT_TIP, answer => {
                resolve(answer);
            });
        });
    }

    /**
     * @param {string[]} args
     * @returns {Promise}
     */
    handleCommand(args) {
        const commandName = args[0];
        const command = this.commands[commandName];
        args = args.slice(1, args.length);

        if (command) {
            return command.execute(args);
        }

        this.log('Unknown command `' + commandName + '`. Try `help`', true);
        return Promise.resolve();
    }

    /**
     * @param {number} port
     * @param {string} address
     * @returns {Promise<void>}
     */
    connectToServer(port, address) {
        return new Promise((resolve, reject) => {
            this.socket = new Socket();
            this.socket.setNoDelay();
            this.socket.on('connect', resolve);
            this.socket.on('close', () => this.log('Connection closed'));
            this.socket.on('error', reject);
            this.socket.on('data', data => this.assemblePacket(data)
                .catch(err => this.log(err, true)));
            this.socket.connect(port, address);
        });
    }

    /**
     * @param {Buffer} rawData
     */
    async assemblePacket(rawData) {
        this.packetBuffer = this.packetBuffer + rawData.toString();

        let separatorPosition;
        while ((separatorPosition = this.packetBuffer.indexOf(PACKET_SEPARATOR)) >= 0) {
            const packetRawData = this.packetBuffer.slice(0, separatorPosition);
            this.packetBuffer = this.packetBuffer.slice(separatorPosition + 1, this.packetBuffer.length);

            let data;
            try {
                data = JSON.parse(packetRawData);
                await this.handleServerPacket(data);
            }
            catch (err) {
                this.log(packetRawData);
                this.log(err, true);
            }
        }
    }

    /**
     * @param {Object} data
     */
    async handleServerPacket(data) {
        const packetType = data.type;

        const packetListener = this.packetListeners[packetType];
        if (packetListener) {
            packetListener(data);
            return;
        }

        const listener = this.dataListeners[0];
        if (listener) {
            this.dataListeners = this.dataListeners.filter(l => l !== listener);
            listener(data);
            return;
        }

        throw 'Packet not handled: ' + packetType;
    }

    /**
     * @param {object} data
     * @param {boolean} expectResponse
     * @returns {Promise<Object|void>}
     */
    sendToServer(data, expectResponse) {
        return new Promise((resolve, reject) => {
            if (this.socket.destroyed) {
                reject('Socket disconnected');
                return;
            }

            if (expectResponse) {
                this.dataListeners.push(resolve);
            }
            this.socket.write(JSON.stringify(data) + PACKET_SEPARATOR);
            if (!expectResponse) {
                resolve();
            }
        });
    }

    /**
     * @param {string} message
     * @param {boolean} [isError]
     */
    log(message, isError) {
        readline.cursorTo(process.stdout, 0);
        process.stdout.write((isError ? 'E: ' : '') + message + '\n' + COMMAND_PROMPT_TIP);
    }

    /**
     * @param {*} data
     */
    handlePingPacket(data) {
        this.sendToServer({type: 'pong'}, false)
            .catch(err => this.log(err, true));
    }

    /**
     * @param {*} data
     */
    handleDisconnectedPacket(data) {
        this.log('Kicked from server. Reason: ' + data.reason);
    }

    /**
     * @param {*} data
     */
    handleConnectionResult(data) {
        if (data.result === 0) {
            this.log('Connected. Authenticating with username ' + this.username + '...');
            this.sendToServer({
                type: 'auth',
                username: this.username
            }, false).catch(err => this.log(err, true));
            return;
        }

        this.log('Connection failed with error: ' + data.error, true);
    }

    /**
     * @param {*} data
     */
    handleAuthResult(data) {
        if (data.result === 0) {
            this.log('Authentication success');
            this.sendToServer({type: 'lobby-info'}, false)
                .catch(err => this.log(err, true));
            return;
        }

        this.log('Authentication failed with error: ' + data.error, true);
    }
}
