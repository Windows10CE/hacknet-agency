import Command from './Command.js';

export default class SelectCommand extends Command {
    get name() {
        return 'select';
    }

    get usage() {
        return '<characterId>';
    }

    execute(args) {
        if (args.length < 1) {
            this.displayUsage();
            return Promise.resolve();
        }
        const target = Number.parseInt(args[0]);

        if (typeof target !== 'number') {
            throw 'Invalid target: ' + args[0];
        }

        return this.select(target);
    }

    /**
     * @param {number} id
     */
    select(id) {
        return new Promise((resolve, reject) => {
            this.client.sendToServer({type: 'lobby-select', character: id}, false);
            resolve();
        });
    }

}
