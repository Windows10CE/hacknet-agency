import Command from './Command.js';

export default class ReadyCommand extends Command {
    get name() {
        return 'ready';
    }

    get usage() {
        return '';
    }

    execute(args) {
        this.client.sendToServer({type: 'lobby-ready'}, false);
        return Promise.resolve();
    }
}
