const DEFAULT_PASSWD_LENGTH = 16;

/**
 * @param {length} [length]
 * @returns {string}
 */
export function generatePassword(length) {
    if (!length) {
        length = DEFAULT_PASSWD_LENGTH;
    }
    // @ts-ignore
    const allowed = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!?#@_-'.shuffle();
    let r = '';
    for (let i = 0; i < length; i++) {
        r += allowed[Math.floor(Math.random() * allowed.length + 1)];
    }
    return r;
}
