import Command from './Command.js';

/** @typedef {import('../../../Character.js').default} Character */

/**
 * @petition to rename this class ListCommand
 */
export default class CommandList extends Command {
    static get cmdName() {
        return 'probe';
    }

    static get help() {
        return 'probe\n\tLists port status on the connected node';
    }

    /**
     * @param {Character} character
     * @param {string[]} args
     */
    static execute(character, args) {
        let string = 'Ports:\n';
        for (const portData of character.currentNode.ports) {
            string += `- ${portData.num} ${portData.protocol}; Status : ${portData.open ? 'OPEN' : 'CLOSED'}`;
        }
        character.writeToTerminal(string);
    }
}
