import Command from './Command.js';

/** @typedef {import('../../../Character.js').default} Character */
/** @typedef {import('../../../node/component/fs/File.js').default} File */
/** @typedef {import('../../../node/component/fs/Directory.js').default} Directory */

/**
 * @petition to rename this class ListCommand
 */
export default class CommandList extends Command {
    static get cmdName() {
        return 'ls';
    }

    static get help() {
        return 'ls (files)\n\tLists all files in the current directory or the specified directory';
    }

    /**
     * @param {Character} character
     * @param {File} file
     * @param {string[]} lines
     */
    static writeFileInfo(character, file, lines) {
        if (file.type === 'dir') {
            for (const subFile of (/** @type {Directory} */ (file)).children) {
                lines.push(`${subFile.name}`);
            }
        }
        else {
            lines.push(`${file.name}`);
        }
    }

    /**
     * @param {Character} character
     * @param {string[]} args
     */
    static execute(character, args) {
        let files = /** @type {File[]} */ ([character.currentDirectory]);
        if (args.length >= 2) {
            const dir = /** @type {Directory} */ (files[0]);
            files = [];
            for (let i = 1; i < args.length; i++) {
                const arg = args[i];
                files = files.concat(dir.getFiles(arg));
            }
            if (files.length === 0) {
                character.writeToTerminal(`${args[0]}: no such file or directory: ${args[1]}`);
                return;
            }
        }

        const lines = [];
        for (const file of files) {
            CommandList.writeFileInfo(character, file, lines);
        }
        character.writeToTerminal(lines.join('\n'));
    }
}
