import Command from './Command.js';

/** @typedef {import('../../../Character.js').default} Character */
/** @typedef {import('../../../node/Node.js').default} Node */

export default class CommandDaemon extends Command {
    static get cmdName() {
        return 'daemon';
    }

    static get help() {
        return 'daemon [DAEMON NAME] [ARGS...]';
    }

    /**
     * @param {Character} character
     * @param {string[]} args
     */
    static execute(character, args) {
        if (args.length < 2) { // TODO default dir is home
            character.writeToTerminal(CommandDaemon.help);
            return;
        }

        const node = character.currentNode;
        const target = character.currentNode.daemons.find((daemon) => daemon.type === args[1]);
        if (target) {
            target.onDaemonRequest(character, args.slice(2));
        }
        else {
            character.writeToTerminal(`${args[0]}: daemon ${args[1]} not found on node ${character.currentNode.displayName}`);
        }
    }
}
