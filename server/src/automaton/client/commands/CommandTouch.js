import Command from './Command.js';

/** @typedef {import('../../../Character.js').default} Character */
import File from '../../../node/component/fs/File.js';

export default class CommandTouch extends Command {
    static get cmdName() {
        return 'touch';
    }

    static get help() {
        return 'touch file\n\tCreates a new file';
    }

    /**
     * @param {Character} character
     * @param {string[]} args
     */
    /**
     * @param {Character} character
     * @param {string[]} args
     */
    static execute(character, args) {
        if (args.length < 2) {
            character.writeToTerminal(CommandTouch.help);
            return;
        }

        const arr = args[1].split('/');
        const fileName = arr.pop();
        const pathStr = arr.join('/');
        if (fileName !== fileName.match(/[A-z0-9_-]+/g)[0]) {
            character.writeToTerminal(`${args[0]}: invalid filename`);
            return;
        }
        let working = character.currentDirectory;
        if (pathStr.length > 0) {
            // @ts-ignore
            working = working.getFile(pathStr);
            if (!working || working.type !== 'dir') {
                character.writeToTerminal(`${pathStr}: directory doesn't exist or isn't a directory`);
                return;
            }
        }
        if (working.getFile(fileName)) {
            return;
        }
        const newFile = new File(working);
        newFile.name = fileName;
        working.addFile(newFile);
    }
}
