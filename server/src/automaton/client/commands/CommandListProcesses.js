import Command from './Command.js';

/** @typedef {import('../../../Character.js').default} Character */

import { PID_MAX } from '../../../node/Node.js';
import { table } from '../../../Logger.js';

export default class CommandListProcesses extends Command {
    static get cmdName() {
        return 'ps';
    }

    static get help() {
        return 'ps\n\tLists all processes active';
    }

    /**
     * @param {Character} character
     * @param {string[]} args
     */
    static execute(character, args) {
        const node = character.homeNode;
        const pidColumn = PID_MAX.toString().length;
        const entries = [['pid', 'process']];
        for (const pid in node.processes) {
            const proc = node.processes[pid];
            entries.push([parseInt(pid), proc.name]);
        }
        character.writeToTerminal(table(entries));
    }
}
