import Command from './Command.js';

/** @typedef {import('../../../Character.js').default} Character */

export default class CommandWorkingDir extends Command {
    static get cmdName() {
        return 'pwd';
    }

    static get help() {
        return 'pwd\n\tPrints the current working directory';
    }

    /**
     * @param {Character} character
     * @param {string[]} args
     */
    static execute(character, args) {
        const node = character.currentNode;
        character.writeToTerminal(`${character.currentDirectory.getAbsolutePath()}@${node.address}:${node.displayName}`);
    }
}
