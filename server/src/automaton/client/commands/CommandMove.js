import Command from './Command.js';

/** @typedef {import('../../../Character.js').default} Character */

export default class CommandMove extends Command {
    static get cmdName() {
        return 'mv';
    }

    static get help() {
        return 'mv source destination \n\tMove a file from the source to the destination';
    }

    /**
     * @param {Character} character
     * @param {string[]} args
     */
    static execute(character, args) {
        if (args.length < 3) {
            character.writeToTerminal(CommandMove.help);
            return;
        }

        const source = character.currentDirectory.getFile(args[1]);
        if (!source) {
            character.writeToTerminal(`${args[0]}: no such file or directory: ${args[1]}`);
            return;
        }

        const arr = args[2].split('/');
        const targetName = arr.pop();
        const pathStr = arr.join('/');

        if (targetName !== targetName.match(/[A-z0-9_-]+/g)[0]) {
            character.writeToTerminal(`${args[0]}: invalid filename: ${targetName}`);
        }
        let working = character.currentDirectory;
        if (pathStr.length > 0) {
            // @ts-ignore
            working = working.getFile(pathStr);
            if (!working || working.type !== 'dir') {
                character.writeToTerminal(`${args[0]}: directory doesn't exist or isn't a directory: ${pathStr}`);
            }
        }
        if (working.getFile(targetName)) {
            character.writeToTerminal(`${args[0]}: destination file already exists: ${args[2]}`);
            return;
        }

        source.delete();
        source.name = targetName;
        working.addFile(source);
    }
}
