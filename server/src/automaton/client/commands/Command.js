/** @typedef {import('../../../Character.js').default} Character */

export const Tokens = {
    FLAGS: ['--', '-'],
    FLAG_ARG: '=',
    FLAG_TAKES_ARG: ':'
};

export default class Command {
    /**
     * @abstract
     * @returns {string} Name of the command
     */
    static get cmdName() {
        throw 'Unimplemented';
    }

    /**
     * @abstract
     * @returns {string} Help line of the command
     */
    static get help() {
        throw 'Unimplemented: There\'s no help for you.';
    }

    /**
     * Executes the command
     * @abstract
     * @param {Character} character Character executing the command
     * @param {string[]} args Arguments from argumentParse()
     */
    static execute(character, args) {
        throw 'Unimplemented';
    }

    /**
     * @param {string} string Argument string to parse
     * @returns {string[]} Arguments array
     */
    static argumentParse(string) {
        const args = [];
        let cur = '';
        let escape = false;
        let group = false;
        let i = 0;
        while (i < string.length) {
            const c = string[i];
            switch (c) {
            case '\\':
                if (escape) {
                    escape = false;
                    cur += c;
                }
                else {
                    escape = true;
                }
                break;
            case '"':
                if (escape) {
                    escape = false;
                    cur += c;
                }
                else {
                    if (group) {
                        group = false;
                        args.push(cur);
                        cur = '';
                    }
                    else {
                        group = true;
                        if (cur !== '') {
                            return null;
                        }
                    }
                }
                break;
            case ' ':
                escape = false;
                if (group) {
                    cur += c;
                }
                else if (cur.length > 0) {
                    args.push(cur);
                    cur = '';
                }
                break;
            default:
                escape = false;
                cur += c;
                break;
            }
            i++;
        }
        if (group) {
            return null;
        }
        if (cur.length > 0) {
            args.push(cur);
        }
        return args;
    }

    /*
        * FLAGS SAMPLE
        * const flags = Command.parseFlags(args, {l: 'list', list: 'list', a: 'all', all: 'all', color: 'color:'});
        * > SAMPLE INPUT
        * >> SAMPLE OUTPUT
        * > ['ls', '-l', '--all']
        * >> { args: ['ls'], flags: {list: null, all: null} }
        * > ['ls', '-a']
        * >> { args: ['ls'], flags: {'all': null} }
        * > ['ls']
        * >> { args: ['ls'], flags: {} }
        * > ['ls', 'mydir']
        * >> { args: ['ls', 'mydir'], flags: {} }
        * > ['ls', 'mydir', '-la']
        * >> { args: ['ls', 'mydir'], flags: {list, 'all'} }
        * > ['ls', 'mydir', '--color=auto]
        * >> { args: ['ls', 'mydir'], flags: {color, }}
    */
    /**
     * @param {string[]} args
     * @returns {Object}
    */
    static flagParse(args, check) {
        const newArgs = [];
        const flags = {};
        let nextFlag = null;

        for (const arg of args) {
            if (nextFlag !== null) {
                flags[check[nextFlag]] = arg;
                nextFlag = null;
                continue;
            }

            let flag = null;

            // Parse flag types
            let isFlag = false;
            for (const flagType of Tokens.FLAGS) {
                if (arg.indexOf(flagType) === 0) {
                    flag = arg.substring(flagType.length);
                    isFlag = true;
                    break;
                }
            }
            if (!isFlag) {
                // Not a flag
                newArgs.push(arg);
                continue;
            }

            // Flag arg parsing
            let flagArg = null;
            // Flag arg token parsing
            const argIndex = flag.indexOf(Tokens.FLAG_ARG);
            if (argIndex > -1) {
                flagArg = flag.slice(argIndex + 1);
                flag = flag.substring(0, argIndex);
            }

            if (flag in check) {
                if (check[flag].endsWith(Tokens.FLAG_TAKES_ARG)) {
                    if (flagArg) {
                        flags[check[flag].slice(0, -Tokens.FLAG_TAKES_ARG.length)] = flagArg;
                    }
                    else {
                        nextFlag = flag;
                    }
                }
                else {
                    flags[check[flag]] = null;
                }
            }
        }

        if (nextFlag !== null) {
            flags[check[nextFlag].slice(0, -Tokens.FLAG_TAKES_ARG.length)] = null;
        }

        return {'args': newArgs, 'flags': flags};
    }
}
