import Command from './Command.js';

/** @typedef {import('../../../Character.js').default} Character */

export default class CommandChangeDir extends Command {
    static get cmdName() {
        return 'cd';
    }

    static get help() {
        return 'cd [directory]\n\tChanges directory to another';
    }

    /**
     * @param {Character} character
     * @param {string[]} args
     */
    static execute(character, args) {
        if (args.length < 2) { // TODO default dir is home
            character.writeToTerminal(CommandChangeDir.help);
            return;
        }

        const target = character.currentDirectory.getFile(args[1]);
        if (!target) {
            character.writeToTerminal(`${args[0]}: no such directory: ${args[1]}`);
            return;
        }
        character.changeDir(target);
    }
}
