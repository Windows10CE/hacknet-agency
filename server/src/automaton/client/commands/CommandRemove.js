import Command from './Command.js';

/** @typedef {import('../../../Character.js').default} Character */

const RemoveErrorCode = {
    OK: 0,
    DIR_NOT_EMPTY: 1,
    ROOT: 2
};

export default class CommandRemove extends Command {
    static get cmdName() {
        return 'rm';
    }

    static get help() {
        return 'rm file\n\tDeletes a file';
    }

    /**
     * @param {Character} character
     * @param {string[]} args
     */
    static execute(character, args) {
        if (args.length < 2) {
            character.writeToTerminal(CommandRemove.help);
            return;
        }

        const files = character.currentDirectory.getFiles(args[1]);
        if (files.length === 0) {
            character.writeToTerminal(`${args[0]}: no such file or directory: ${args[1]}`);
            return;
        }
        for (const file of files) {
            if (file.getParents().length === 1 && ['bin', 'log', 'sys', 'home'].includes(file.name)) {
                character.writeToTerminal(`${args[0]}: cannot remove '${file.name}': is essential folder`);
                continue;
            }

            switch (file.delete()) {
            case RemoveErrorCode.ROOT:
                character.writeToTerminal(`${args[0]}: cannot remove '${file.name}': is root directory`);
                break;
            case RemoveErrorCode.DIR_NOT_EMPTY:
                character.writeToTerminal(`${args[0]}: cannot remove '${file.name}': is non-empty directory`);
                break;
            default:
                break;
            }
        }
    }
}
