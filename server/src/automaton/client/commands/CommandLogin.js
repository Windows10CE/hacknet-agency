import Command from './Command.js';

/** @typedef {import('../../../Character.js').default} Character */
/** @typedef {import('../../../node/Node.js').default} Node */

export default class CommandLogin extends Command {
    static get cmdName() {
        return 'login';
    }

    static get help() {
        return 'login [username password]';
    }

    /**
     * @param {Character} character
     * @param {string[]} args
     */
    static execute(character, args) {
        const node = character.currentNode;
        if (args.length >= 2 && (args[1] === '-ls' || args[1] === '--ls')) {
            let message = `Known accounts for ${node.displayName}:\n`;
            for (const acct of character.knownAccounts.filter((acct) => acct.node === node)) {
                message = message + `\t${acct.acctName}: ${acct.acctPassword}\n`;
            }
            character.writeToTerminal(message, '');
        }
        else if (args.length >= 3) {
            if (!node.tryLogin(character, args[1], args[2])) {
                character.writeToTerminal('Couldn\'t log in.');
            }
            else {
                character.writeToTerminal(`Logged in as ${args[1]}.`);
            }
        }
        else {
            node.onLogoff(character);
            character.writeToTerminal(`Logged off from ${node.displayName} at ${node.address}`);
        }
    }
}
