import Command from './Command.js';

/** @typedef {import('../../../Character.js').default} Character */

export default class CommandKill extends Command {
    static get cmdName() {
        return 'kill';
    }

    static get help() {
        return 'kill pid\n\tStops a program with that process ID running';
    }

    /**
     * @param {Character} character
     * @param {string[]} args
     */
    static execute(character, args) {
        if (args.length < 2) {
            character.writeToTerminal(CommandKill.help);
            return;
        }

        const node = character.homeNode;
        const pid = parseInt(args[1]);
        if (isNaN(pid)) {
            character.writeToTerminal(`${args[0]}: illegal pid: ${args[1]}`);
            return;
        }

        if (!node.processes[pid]) {
            character.writeToTerminal(`${args[0]}: no such process ${pid}`);
            return;
        }
        
        node.processes[pid].kill();
    }
}
