import Command from './Command.js';

/** @typedef {import('../../../Character.js').default} Character */
import Directory from '../../../node/component/fs/Directory.js';

export default class CommandMakeDir extends Command {
    static get cmdName() {
        return 'mkdir';
    }

    static get help() {
        return 'mkdir file\n\tCreates a new directory';
    }

    /**
     * @param {Character} character
     * @param {string[]} args
     */
    /**
     * @param {Character} character
     * @param {string[]} args
     */
    static execute(character, args) {
        if (args.length < 2) {
            character.writeToTerminal(CommandMakeDir.help);
            return;
        }

        const arr = args[1].split('/');
        const fileName = arr.pop();
        const pathStr = arr.join('/');
        if (fileName !== fileName.match(/[A-z0-9_-]+/g)[0]) {
            character.writeToTerminal(`${args[0]}: invalid filename`);
        }
        let working = character.currentDirectory;
        if (pathStr.length > 0) {
            // @ts-ignore
            working = working.getFile(pathStr);
            if (!working || working.type !== 'dir') {
                character.writeToTerminal(`${args[0]}: ${pathStr}: directory doesn't exist or isn't a directory`);
            }
        }
        if (working.getFile(fileName)) {
            return;
        }
        const newFile = new Directory(working);
        newFile.name = fileName;
        working.addFile(newFile);
    }
}
