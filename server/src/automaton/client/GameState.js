import State from './State.js';

import {ClientStatus} from '../../Client.js';

import Command from './commands/Command.js';
import CommandConnect from './commands/CommandConnect.js';
import CommandDisconnect from './commands/CommandDisconnect.js';
import CommandChangeDir from './commands/CommandChangeDir.js';
import CommandWorkingDir from './commands/CommandWorkingDir.js';
import CommandList from './commands/CommandList.js';
import CommandTouch from './commands/CommandTouch.js';
import CommandRemove from './commands/CommandRemove.js';
import CommandMakeDir from './commands/CommandMakeDir.js';
import CommandMove from './commands/CommandMove.js';
import CommandLogin from './commands/CommandLogin.js';
import CommandDaemon from './commands/CommandDaemon.js';
import CommandListProcesses from './commands/CommandListProcesses.js';
import CommandKill from './commands/CommandKill.js';
import CommandProbe from './commands/CommandProbe.js';

import PacketEnterWorld from '../../network/packet/general/PacketEnterWorld.js';
import PacketTerminalMessage from '../../network/packet/game/PacketTerminalMessage.js';

import log, {LogType} from '../../Logger.js';

/** @typedef {import('../../Client.js').default} Client */
/** @typedef {import('../../node/component/fs/special/Executable.js').default} Executable */

export default class GameState extends State {
    listeners = {
        'term-input': (packet) => this.onTermInputPacket(packet)
    };

    /**
     * @type {Object.<string, typeof Command>}
     */
    commands = {
        'connect': CommandConnect,
        'disconnect': CommandDisconnect,
        
        'cd': CommandChangeDir,
        'pwd': CommandWorkingDir,
        'ls': CommandList,
        
        'touch': CommandTouch,
        'mkdir': CommandMakeDir,
        
        'rm': CommandRemove,
        'mv': CommandMove,
        
        'login': CommandLogin,
        'daemon': CommandDaemon,
        'probe': CommandProbe,

        'ps': CommandListProcesses,
        'kill': CommandKill,
    };

    /**
     * @param {Client} client
     */
    constructor(client) {
        super(client);
    }

    enter() {
        this.client.status = ClientStatus.INGAME;

        const pack = new PacketEnterWorld();
        this.client.send(pack);
    }

    exit() {

    }

    /**
     * Called whenever the client received a packet of type 'term-input'
     * @param {*} packet
     */
    onTermInputPacket(packet) {
        const character = this.client.character;
        
        if (!packet.content || typeof packet.content !== 'string') {
            return;
        }
        const args = Command.argumentParse(packet.content);
        if (this.commands[args[0]]) {
            try {
                this.commands[args[0]].execute(character, args);
            }
            catch (e) {
                log(LogType.WARNING, `Failed to run command ${args[0]}: ${e} ${e.stack}`);
            }
        }
        else {
            let file;
            if (args[0].startsWith('./') && character.currentNode === character.homeNode) {
                file = character.currentDirectory.getChild(args[0]);
                if (!file) {
                    character.writeToTerminal(`File ${args[0]} was not found.`);
                    return;
                }
            }
            else {
                file = character.homeNode.getBinFolder().getChild(args[0]);
            }

            if (file) {
                if (file.type === 'exe') {
                    const executable = /** @type {Executable} */ (file);
                    character.homeNode.runExecutable(executable, character, args);
                }
                else {
                    character.writeToTerminal(`File ${args[0]} isn't a valid executable file.`);
                }
            }
            else {
                character.writeToTerminal(`Command not found: ${args[0]}`);
            }
        }
    }
}
