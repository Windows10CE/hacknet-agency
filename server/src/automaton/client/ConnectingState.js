import State from './State.js';

import LobbyState from './LobbyState.js';

import PacketAuthResult from '../../network/packet/system/PacketAuthResult.js';

import log, { LogType } from '../../Logger.js';

/** @typedef {import('../../Client.js').default} Client */

const ConnectionStatus = {
    IDENTIFYING: 0,
    AUTHENTICATED: 1
};

const AUTH_TIMEOUT = 5;

export default class ConnectingState extends State {
    /** @type {NodeJS.Timeout} */
    timeout = null;

    status = ConnectionStatus.IDENTIFYING;

    listeners = {'auth': (packet) => this.onIdentificationPacket(packet)};

    /**
     * @param {Client} client
     */
    constructor(client) {
        super(client);
    }

    enter() {
        this.timeout = setTimeout(() => {
            this.client.disconnect('Client did not authenticate');
        }, AUTH_TIMEOUT * 1000);
    }

    /**
     * Sends message to client telling it has been authenticated
     */
    sendAuthenticationResult() {
        this.status = ConnectionStatus.AUTHENTICATED;
        const packet = new PacketAuthResult(PacketAuthResult.Result.ACCEPTED, '');
        this.client.send(packet);

        this.client.setState(new LobbyState(this.client));
    }

    /**
     * Called when client sends 'auth' packet
     * @param {{ username: string }} packet
     */
    onIdentificationPacket(packet) {
        if (this.status !== ConnectionStatus.IDENTIFYING) {
            return;
        }

        if (typeof packet.username !== 'string' || !packet.username || packet.username.length < 3) {
            return;
        }
        // Handle authentication
        this.client.username = packet.username;

        // Client has been authenticated
        clearTimeout(this.timeout);
        this.sendAuthenticationResult();
        
        log(LogType.INFO, `Client authenticated with username ${this.client.username}`);
    }
}
