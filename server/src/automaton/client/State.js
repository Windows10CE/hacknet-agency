/** @typedef {import('../../Client.js').default} Client */

export default class State {
    /** @type {Client} */
    client;

    /** @type {Object.<string, (packet: *) => void>} */
    listeners = {};

    /**
     * @param {Client} client
     */
    constructor(client) {
        this.client = client;
    }

    /**
     * State is entered
     */
    enter() {

    }

    /**
     * State is left
     */
    exit() {

    }

    /**
     * Handles received packets.
     * @param {*} packet
     */
    onPacketReceive(packet) {
        const listener = this.listeners[packet.type];
        if (listener) {
            listener(packet);
        }
    }
}
