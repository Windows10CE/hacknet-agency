import net from 'net';
import PacketConnectionResult from './network/packet/system/PacketConnectionResult.js';
import PacketClientsInfo, {ClientInfo} from './network/packet/general/PacketClientsInfo.js';

import Client, {ClientStatus} from './Client.js';

/** @typedef {import('./World.js').default} World */
import WorldLoader from './WorldLoader.js';
import log, {LogType} from './Logger.js';

export const ServerStatus = {
    STOPPING: -1,
    PREPARING: 0,
    LOBBY: 1,
    IN_GAME: 2
};

export default class Server {
    /** @type {net.Server} */
    server;

    /** @type {number} */
    status = ServerStatus.PREPARING;

    /** @type {World} */
    world;

    /** @type {Client[]} */
    clients = [];

    lastClientId = 0;


    constructor() {

    }

    init() {
        // World loading
        const extensionPath = './exts/sample';
        this.world = WorldLoader(extensionPath);
        if (this.world === null) {
            log(LogType.ERROR, 'Unable to load world, stopping.');
            return;
        }
        this.world.create();
        this.world.init();

        this.world.startMainLoop();

        // Status
        this.status = ServerStatus.LOBBY;

        // Network
        this.server = new net.Server(null, (socket) => this.onConnection(socket));
        this.server.listen(3500, () => {
            log(LogType.INFO, 'Server listening on port 3500.');
        });
    }

    /**
     * @param {net.Socket} socket
     */
    onConnection(socket) {
        // @ts-ignore
        log(LogType.INFO, `Connection attempt from ${socket.address().address}.`);
        switch (this.status) {
        case ServerStatus.STOPPING:
            {
                const packet = new PacketConnectionResult(PacketConnectionResult.Result.ERROR, 'Server is stopping');
                socket.write(packet.complete());
            }
            break;
        case ServerStatus.PREPARING:
            {
                const packet = new PacketConnectionResult(PacketConnectionResult.Result.ERROR, 'Server is not ready');
                socket.write(packet.complete());
            }
            break;
        case ServerStatus.LOBBY:
            {
                this.acceptConnection(socket);
            }
            break;
        case ServerStatus.IN_GAME:
            {
                this.acceptConnection(socket);
            }
            break;
        default:
            {
                const packet = new PacketConnectionResult(PacketConnectionResult.Result.ERROR, 'Oops.');
                socket.write(packet.complete());
            }
            break;
        }
    }

    /**
     * @param {net.Socket} socket
     */
    acceptConnection(socket) {
        if (this.clients.length > this.world.info.characterAmount.max) {
            const packet = new PacketConnectionResult(PacketConnectionResult.Result.ERROR, 'Server is full');
            socket.write(packet.complete());
        }
        else {
            const client = new Client(this, socket, this.lastClientId++);
            this.clients.push(client);
            client.init();
            const packet = new PacketConnectionResult(PacketConnectionResult.Result.ACCEPTED, '');
            client.send(packet);
        }
    }

    /**
     * @param {Client} client
     */
    disconnect(client) {
        this.clients = this.clients.filter((c) => c !== client);
    }

    onClientsUpdate() {
        const packet = this.getClientsInfoPacket();
        for (const client of this.clients.filter((client) => client.status > ClientStatus.CONNECTING)) {
            client.send(packet);
        }
    }

    /**
     * @returns {PacketClientsInfo}
     */
    getClientsInfoPacket() {
        const result = [];
        for (const client of this.clients.filter((client) => client.status > ClientStatus.CONNECTING)) {
            result.push(client.getDisplayData());
        }
        return new PacketClientsInfo(result);
    }

    getWorld() {
        return this.world;
    }
}
