import Character from './Character.js';
/** @typedef {import('./World.js')} World */
import log, {LogType} from './Logger.js';

/**
 * @todo
 * @param {World} world
 * @returns {Character}
 */
export function generateFromDefault(world) {
    return null;
}

export default function(world, data, id) {
    if (data === null || data === undefined) {
        return null;
    }

    const result = new Character(world, id);
    result.name = data.name;
    result.dependencies.homeNode = data.home_node;
    result.dependencies.mailServer = data.mail_server;

    log(LogType.VERBOSE, `Loaded character ${result.name}`);

    return result;
}
