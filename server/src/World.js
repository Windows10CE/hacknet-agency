/** @typedef {import('./Character.js').default} Character */
/** @typedef {import('./node/Node.js').default} Node */
/** @typedef {import('./mission/Mission.js').default} Mission */

import log, {LogType} from './Logger.js';
import PacketWorldInfo, { WorldInfo } from './network/packet/general/PacketWorldInfo.js';

const TICK_SPEED = 1000 / 40;

export default class World {
    info = {
        path: '',
        name: '',
        characterAmount: {min: 1, max: 8}
    };

    /** @type {*} */
    dependencies = {
        startingMissions: []
    };

    /** @type {Node[]} */
    nodes = [];

    /** @type {Mission[]} */
    missions = [];

    /** @type {{character: Character, mission: Mission}[]} */
    startedMissions = [];

    /** @type {{character: Character, mission: Mission}[]} */
    pendingMissions = [];

    /** @type {Character} */
    defaultCharacter = null;

    /** @type {Character[]} */
    characters = [];

    constructor() {
        this.world = undefined;
    }

    create() {
        for (const node of this.nodes) {
            node.create();
        }
    }

    init() {
        for (const node of this.nodes) {
            node.init();
        }
        this.startMissions();
    }

    lastTick = 0;
    curTick = 0;
    ticks = 0;
    tickTime = 0;
    startMainLoop() {
        this.lastTick = Date.now();
        this.curTick = Date.now();
        this.mainLoop();
    }

    mainLoop() {
        this.ticks++;
        this.curTick = Date.now();

        this.tick((this.curTick - this.lastTick) / 1000);

        this.lastTick = this.curTick;
        const curTime = Date.now();
        const planned = this.curTick + TICK_SPEED;
        
        setTimeout(() => this.mainLoop(), planned - curTime);

        this.tickTime = this.tickTime * 0.5 + (Date.now() - this.lastTick) * 0.5;
        if (this.ticks % 800 === 400) {
            log(LogType.DEBUG, `Average tickTime: ${this.tickTime}ms`);
        }
    }
    
    /**
     * @param {number} delta
     */
    tick(delta) {
        for (const data of this.startedMissions) {
            const mission = data.mission;
            if (mission.auto) {
                mission.check(data.character, null);
            }
        }
        for (const node of this.nodes) {
            node.tick(delta);
        }
    }

    /**
     * @returns {boolean}
     */
    resolve() {
        for (const node of this.nodes) {
            if (!node.resolve(this)) {
                log(LogType.ERROR, `Resolving node ${node.id} failed.`);
                return false;
            }
        }

        for (const mission of this.missions) {
            if (!mission.resolve(this)) {
                log(LogType.ERROR, `Resolving mission ${mission.id} failed.`);
                return false;
            }
        }

        for (const character of this.characters) {
            if (!character.resolve(this)) {
                log(LogType.ERROR, `Resolving character ${character.name} failed.`);
                return false;
            }
        }

        for (const missionData of this.dependencies.startingMissions) {
            const id = missionData.id;
            const characterData = missionData.character;
            const mission = this.getMissionFromId(id);
            if (!mission) {
                log(LogType.ERROR, `Resolving starting mission ${id} failed.`);
                return false;
            }
            
            const character = this.getCharacterFromName(characterData);
            if (!character) {
                log(LogType.ERROR, `Resolving character identifier ${characterData} failed.`);
                return false;
            }
            this.registerMission({mission: mission, character: character});
        }

        return true;
    }

    /**
     * @param {{character: Character, mission: Mission}} mission
     */
    registerMission(mission) {
        this.pendingMissions.push(mission);
    }

    startMissions() {
        let mission;
        // eslint-disable-next-line no-cond-assign
        while (mission = this.pendingMissions.pop()) {
            this.startedMissions.push(mission);
            mission.mission.launch(mission.character);
        }
    }

    /**
     * @param {Character} character
     * @param {Mission} mission
     */
    missionComplete(character, mission) {
        this.startedMissions = this.startedMissions.filter((missionData) => missionData !== {character: character, mission: mission});
    }

    /**
     * @returns {WorldInfo}
     */
    getDisplayData() {
        const r = [];
        for (const character of this.characters) {
            r.push(character.getCharacterInfo());
        }
        return new WorldInfo(this.info.name, r);
    }

    /**
     * @returns {PacketWorldInfo}
     */
    getWorldInfoPacket() {
        const packet = new PacketWorldInfo(this.getDisplayData());
        return packet;
    }

    getCharacters() {
        return this.characters;
    }

    /**
     * @param {number} id
     */
    getCharacterFromId(id) {
        return this.characters.find((char) => char.id === id);
    }

    /**
     * @param {string} name
     */
    getCharacterFromName(name) {
        return this.characters.find((char) => char.name === name);
    }
    
    /**
     * @param {string} id
     * @param {*[]} arr
     * @returns {*}
     */
    getFromId(id, arr) { return arr.find(obj => obj.id === id); }
    
    /**
     * @param {string} id
     * @returns {Node}
     */
    getNodeFromId(id) { return this.getFromId(id, this.nodes); }
    
    /**
     * @param {string} id
     * @returns {Mission}
     */
    getMissionFromId(id) { return this.getFromId(id, this.missions); }

    /**
     * @param {string} address
     */
    getNodeFromAddress(address) {
        return this.nodes.find(node => node.address === address);
    }
}
