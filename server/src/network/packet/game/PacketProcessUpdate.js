import Packet from '../Packet.js';

export default class PacketProcessUpdate extends Packet {
    type = 'process-update';

    pid = 0;

    procName = '';
    
    procRAM = 0;
    
    procDead = false;

    curRAM = 0;

    /**
     * @param {string} addr
     * @param {number} pid
     * @param {string} procName
     * @param {number} procRAM
     * @param {boolean} procDead
     * @param {number} curRAM
     */
    constructor(addr, pid, procName, procRAM, procDead, curRAM) {
        super();
        this.addr = addr;
        this.pid = pid;
        this.procName = procName;
        this.procRAM = procRAM;
        this.procDead = procDead;
        this.curRAM = curRAM;
    }
    
    /**
     * @param {PacketProcessUpdate} packet
     * @returns {*}
     */
    static serialize(packet) {
        return { addr: packet.addr, pid: packet.pid, procName: packet.procName, procRAM: packet.procRAM,
            procDead: packet.procDead, curRAM: packet.curRAM };
    }
}
