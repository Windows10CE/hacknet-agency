import PacketDaemon from './PacketDaemon.js';

export default class PacketMailRead extends PacketDaemon {
    type = 'mail-read';

    mail;

    /**
     * @param {*} mail
     */
    constructor(mail) {
        super('mail');
        this.mail = mail;
    }
    
    /**
     * @param {PacketMailRead} packet
     * @returns {*}
     */
    static serialize(packet) {
        return { mail: packet.mail };
    }
}
