import PacketDaemon from './PacketDaemon.js';

export default class PacketMailFetch extends PacketDaemon {
    type = 'mail-fetch';

    mailListing;

    /**
     * @param {*[]} mailHeaders
     */
    constructor(mailHeaders) {
        super('mail');
        this.mailListing = mailHeaders;
    }
    
    /**
     * @param {PacketMailFetch} packet
     * @returns {*}
     */
    static serialize(packet) {
        return { mailListing: packet.mailListing };
    }
}
