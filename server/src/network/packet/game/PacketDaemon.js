import Packet from '../Packet.js';

export default class PacketDaemon extends Packet {
    type = 'daemon';

    daemonType = '';

    /**
     * @param {string} daemonType
     */
    constructor(daemonType) {
        super();
        this.daemonType = daemonType;
    }
    
    /**
     * @param {PacketDaemon} packet
     * @returns {*}
     */
    static serialize(packet) {
        return { daemonType: packet.daemonType };
    }
}
