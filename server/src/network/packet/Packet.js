export const PACKET_SEPARATOR = '\0';

export default class Packet {
    type = 'none';

    constructor() {

    }

    /**
     * Handles what should be sent via network.
     * Must be implemented in derived classes.
     * @virtual
     * @param {Packet} packet Packet Structure to fetch information from
     * @returns {*}
     */
    static serialize(packet) {
        return {type: packet.type};
    }

    /**
     * Handles chain serialization of derived packets.
     * @private
     * @returns {*}
     */
    chainSerialize() {
        // @ts-ignore
        let preSerialized = this.constructor.serialize(this);
        let proto = this;
        do {
            proto = Object.getPrototypeOf(proto.constructor).prototype;
            // @ts-ignore
            preSerialized = Object.assign(preSerialized, proto.constructor.serialize(this));
        } while (proto.constructor.name !== 'Packet');
        return preSerialized;
    }

    /**
     * @virtual
     * @param {Object} data Packet data
     * @returns {*} Packet binary format
     */
    build(data) {
        return JSON.stringify(data) + PACKET_SEPARATOR;
    }

    /**
     * @returns {*} Packet binary format
     */
    complete() {
        return this.build(this.chainSerialize());
    }
}
