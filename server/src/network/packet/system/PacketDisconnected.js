import Packet from '../Packet.js';

/**
 * The client was disconnected by the server
 */
export default class PacketDisconnected extends Packet {
    type = 'disconnected';

    reason = '';

    /**
     * @param {string} reason
     */
    constructor(reason) {
        super();
        this.reason = reason;
    }

    /**
     * @param {PacketDisconnected} packet
     * @returns {*}
     */
    static serialize(packet) {
        return {reason: packet.reason};
    }
}
