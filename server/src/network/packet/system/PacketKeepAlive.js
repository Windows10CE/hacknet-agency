import Packet from '../Packet.js';

/**
 * Connection heartbeat request
 *
 * The client must respond to acknowledge that the connection is still alive.
 * If the request times out, the client is kicked.
 */
export default class PacketKeepAlive extends Packet {
    type = 'ping';

    constructor() {
        super();
    }

    /**
     * @param {PacketKeepAlive} packet
     * @returns {*}
     */
    // @ts-ignore
    static serialize(packet) {
        return {};
    }
}
