import Packet from '../Packet.js';

/** @typedef {import('../../../World.js').default} World */

export default class PacketWorldInfo extends Packet {
    type = 'world-info';

    /** @type {WorldInfo} */
    clientInfos = null;

    /**
     * @param {WorldInfo} worldInfo
     */
    constructor(worldInfo) {
        super();
        this.worldInfo = worldInfo;
    }
    
    /**
     * @param {PacketWorldInfo} packet
     * @returns {{ worldInfo: WorldInfo }}
     */
    static serialize(packet) {
        return { worldInfo: packet.worldInfo };
    }
}

export class WorldInfo {
    name = '';

    /** @type {any[]} */
    characters = [];

    /**
     * @param {string} name
     * @param {{name:string, id:number}[]} characters
     */
    constructor(name, characters) {
        this.name = name;
        this.characters = characters;
    }
}
