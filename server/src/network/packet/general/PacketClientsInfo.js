import Packet from '../Packet.js';

/** @typedef {import('../../../Server.js').default} Server */

export default class PacketClientsInfo extends Packet {
    type = 'clients-info';

    /** @type {ClientInfo[]} */
    clientInfos = [];

    /**
     * @param {ClientInfo[]} clientInfos
     */
    constructor(clientInfos) {
        super();
        this.clientInfos = clientInfos;
    }
    
    /**
     * @param {PacketClientsInfo} packet
     * @returns {{ clientInfos: ClientInfo[] }}
     */
    static serialize(packet) {
        return { clientInfos: packet.clientInfos };
    }
}

export class ClientInfo {
    /** @type {number} */
    id;

    /** @type {string} */
    username;

    /** @type {string} */
    characterName;

    /** @type {boolean} */
    playing;

    /**
     * @param {number} id
     * @param {string} username
     * @param {string} characterName
     * @param {boolean} playing
     */
    constructor(id, username, characterName, playing) {
        this.id = id;
        this.username = username;
        this.characterName = characterName;
        this.playing = playing;
    }
}
