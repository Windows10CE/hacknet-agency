/** @typedef {import('../World.js').default} World */
/** @typedef {import('../Character.js').default} Character */
/** @typedef {import('./component/fs/Directory.js').default} Directory */
/** @typedef {import('./component/daemon/Daemon.js').default} Daemon */

/** @typedef {import('./component/fs/special/Executable.js').default} Executable */

/** @typedef {{name: string, password: string, home: string, admin: boolean}} Account */

import log, {LogType} from '../Logger.js';

import CharTracker from './component/CharTracker.js';

import Programs from './component/process/Programs.js';
import Process from './component/process/Process.js';
import { generatePassword } from '../misc/Util.js';
import Character from '../Character.js';
import PacketProcessUpdate from '../network/packet/game/PacketProcessUpdate.js';

export const PID_MAX = 32768;
export const PID_MIN = 2;

/**
 * @param {World} [world]
 * @returns {string}
 */
export function generateAddress(world) {
    const forbidden = [0, 10, 100];
    let result = '';
    do {
        let leading = 0;
        while (forbidden.indexOf(leading) > -1) {
            leading = Math.floor(Math.random() * 127);
        }
        result = `${leading}.${Math.floor(Math.random() * 256)}.${Math.floor(Math.random() * 256)}.${Math.floor(Math.random() * 256)}`;
    } while (world && world.getNodeFromAddress(result));
    return result;
}

export default class Node {
    /** @type {*} */
    dependencies = {
        links: []
    }

    /** @type {string} */
    id;

    /** @type {string} */
    address;

    /** @type {string} */
    displayName;

    /** @type {Character} */
    owner;

    /** @type {number} */
    proxy;

    portsToCrack = 0;

    /** @type {*} */
    firewall = {
        solution: '',
        additionalTime: 0,
        level: 0
    }

    /** @type {Node[]} */
    links = [];

    /** @type {*} */
    monitor = {
        type: '',
        traceTime: null
    }

    /** @type {Daemon[]} */
    daemons = [];

    /** @type {Account[]} */
    accounts = [];

    /** @type {Directory} */
    root;

    /** @type {World} */
    world;

    //=====| GAME DATA |=====\\
    /** @type {Object.<number, Process>} */
    processes = {};

    baseRam = 1000;
    currentRam = 1000;

    /** @type {number} */
    nextPID = PID_MIN;

    /** @type {{num: number, protocol: string, open: boolean}[]} */
    ports = [];

    /** @type {Object<number, CharTracker>} */
    trackedCharacters = {};

    /** @param {World} world */
    constructor(world) {
        this.world = world;
    }

    create() {
        for (const daemon of this.daemons) {
            daemon.create();
        }

        if (!this.root.getChild('log')) {
            this.root.createDirectory('log');
        }
        if (!this.root.getChild('home')) {
            this.root.createDirectory('home');
        }
        if (!this.root.getChild('bin')) {
            this.root.createDirectory('bin');
        }
        if (!this.root.getChild('sys')) {
            this.root.createDirectory('sys');
        }

        const sysDir = /** @type {Directory} */ (this.root.getChild('sys'));
        if (!sysDir.getChild('xserver.sys')) {
            sysDir.createFile('xserver.sys', 'SYSTEM SERVER');
        }

        if (!this.accounts.find((acctInfo) => acctInfo.admin === true)) {
            this.accounts.push({
                name: 'admin',
                password: generatePassword(16),
                home: '/',
                admin: true
            });
        }
    }

    init() {
        for (const daemon of this.daemons) {
            daemon.init();
        }
    }

    /**
     * @param {World} world
     * @returns {boolean} Whether the node is valid in its world
     */
    resolve(world) {
        let success = true;
        // Create address if there's none provided
        if (!this.address) {
            this.address = generateAddress(world);
        }

        // Resolve the node links
        for (let i = 0; i < this.dependencies.links.length; i++) {
            this.links[i] = world.getNodeFromId(this.dependencies.links[i]);
            if (!this.links[i]) {
                log(LogType.ERROR, `Failed to resolve link ${this.dependencies.links[i]} for node ${this.id}`);
                success = false;
            }
        }
        
        return success;
    }

    /**
     * @param {Process} process Process to update signature
     * @param {boolean} death Whether the process has been killed
     */
    onProcessUpdate(process, death) {
        const chars = /** @type {CharTracker[]} */
            (Object.values(this.trackedCharacters).filter(tc =>
                ((tc.online === true || tc.character === this.owner) && tc.character.pilot)));
        this.currentRam = this.baseRam;
        for (const proc of /** @type {Process[]} */ (Object.values(this.processes))) {
            this.currentRam -= proc.ramUsage;
        }
        const pack = new PacketProcessUpdate(this.address, process.pid, process.name, process.ramUsage, death, this.currentRam);
        for (const char of chars) {
            char.character.pilot.send(pack);
        }
    }

    /**
     * @param {Executable} executable
     * @param {Character} character
     * @param {string[]} args
     */
    runExecutable(executable, character, args) {
        const target = executable.target;
        if (Programs[target]) {
            // Create process
            const process = /** @type {Process} */ (new Programs[target](this.nextPID, executable.name, this, character));
            this.processes[process.pid] = process;
            if (process.onStart(args)) {
                // Increment next PID
                ++this.nextPID;
                if (this.nextPID >= PID_MAX) {
                    this.nextPID = PID_MIN;
                }
                this.onProcessUpdate(process, false);
            }
        }
    }

    /**
     * @param {number} delta
     */
    tick(delta) {
        for (const pid in this.processes) {
            if (!this.processes[pid].killed) {
                this.processes[pid].onTick(delta);
            }
        }
    }

    /**
     * @param {number} pid
     */
    processKilled(pid) {
        const proc = this.processes[pid];
        delete this.processes[pid];
        this.onProcessUpdate(proc, true);
    }

    isVulnerable() {
        return this.ports.filter((portData) => portData.open).length >= this.portsToCrack;
    }

    /**
     * @returns {Directory} Root Directory
     */
    getRoot() {
        return this.root;
    }

    /**
     * @returns {Directory} Bin Directory
     */
    getBinFolder() {
        return /** @type {Directory} */ (this.root.getChild('bin'));
    }

    /**
     * @param {number} num Port number
     */
    getPort(num) {
        return this.ports.find((p) => p.num === num);
    }

    /**
     * @param {Character} character
     */
    onConnect(character) {
        character.currentNode = this;
        const tracker = this.getCharacterTracker(character);
        tracker.online = true;
    }

    /**
     * @param {Character} character
     * @param {string} acctName
     * @param {string} password
     * @returns {boolean}
     */
    tryLogin(character, acctName, password) {
        const targetAcctId = this.accounts.findIndex((acct) => acct.name === acctName);
        const targetAcct = this.accounts[targetAcctId];
        if (!targetAcct) {
            return false;
        }
        if (targetAcct.password !== password) {
            return false;
        }
        
        this.onLogin(character, targetAcctId);
        return true;
    }

    /**
     * @param {Character} character
     */
    grantAdmin(character) {
        let targetAcctId = this.accounts.findIndex((acct) => acct.name === 'admin');
        if (targetAcctId === -1) {
            targetAcctId = this.accounts.findIndex((acct) => acct.admin);
            if (targetAcctId === -1) {
                log(LogType.WARNING, `${character.name} got admin access to ${this.id} node yet no admin account was available.`);
                return;
            }
        }
        const acct = this.accounts[targetAcctId];
        character.writeToTerminal(`Admin Access granted using account '${acct.name}'`);
        character.knownAccounts.push({node: this, acctName: acct.name, acctPassword: acct.password});
        this.onLogin(character, targetAcctId);
    }

    /**
     * @param {Character} character
     * @param {number} acctId
     */
    onLogin(character, acctId) {
        const tracker = this.getCharacterTracker(character);
        tracker.currentAccount = acctId;
    }

    /**
     * @param {Character} character
     */
    onDisconnect(character) {
        const tracker = this.getCharacterTracker(character);
        if (tracker.currentAccount === -1) {
            this.stopTracking(character);
        }
    }

    /**
     * @param {Character} character
     */
    onLogoff(character) {
        this.trackedCharacters[character.id].currentAccount = -1;
    }

    /**
     * @param {Character} character
     * @returns {CharTracker}
     */
    startTracking(character) {
        const tracker = new CharTracker(character);
        this.trackedCharacters[character.id] = tracker;
        return tracker;
    }

    /**
     * @param {Character} character
     */
    stopTracking(character) {
        delete this.trackedCharacters[character.id];
    }

    /**
     * @param {Character} character
     * @returns {CharTracker}
     */
    getCharacterTracker(character) {
        let tracker = this.trackedCharacters[character.id];
        if (!tracker) {
            tracker = this.startTracking(character);
        }
        return tracker;
    }

    /**
     * @param {Character} character
     * @returns {number}
     */
    getCurrentAccountId(character) {
        return this.trackedCharacters[character.id].currentAccount;
    }

    /**
     * @param {Character} character
     * @returns {Account}
     */
    getCurrentAccount(character) {
        const id = this.getCurrentAccountId(character);
        if (id === -1) {
            return null;
        }
        return this.accounts[id];
    }

    /**
     * @param {string} accountName
     * @returns {Account}
     */
    getAccount(accountName) {
        return this.accounts.find((account) => account.name === accountName);
    }
}
