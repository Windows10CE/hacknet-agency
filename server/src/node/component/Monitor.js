/** @typedef {import('../Node.js').default} Node */
/** @typedef {import('../../Character.js').default} Character */

export default class Monitor {
    // functions: onTraceEnd, onTraceStart
    // what would be useful for a sysadmin to have?
    // and ext makers
    // file delete
    // file creation

    /** @type {Node} */
    node;

    /**
     * @param {Node} node
     */
    constructor(node) {
        this.node = node;
    }

    /**
     * @param {Character} character
     */
    onIllegalActivity(character) {
        this.traceStart(character);
    }

    /**
     * @param {Character} character
     */
    traceStart(character) { // store who the fuck is connected
        //character.world.characters.find(character => character. == this.node.address);
    }

    traceEnd() {} // how did the trace end? dc or traced?
}
