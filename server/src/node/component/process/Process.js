/** @typedef {import('../../Node.js').default} Node */
/** @typedef {import('../../../Character.js').default} Character */

import log, {LogType} from '../../../Logger.js';

export default class Process {
    ramUsage = 0;

    /** @type {number} */
    pid;

    /** @type {string} */
    name;

    /** @type {Node} */
    node;

    /** @type {Character} */
    character;

    /** @type {boolean} */
    killed = false;

    /**
     * @param {number} pid
     * @param {string} name
     * @param {Node} node
     * @param {Character} character
     */
    constructor(pid, name, node, character) {
        this.pid = pid;
        this.name = name;
        this.node = node;
        this.character = character;
    }

    /**
     * @param {string[]} args
     * @returns {boolean}
     */
    onStart(args) {
        log(LogType.VERBOSE, `Process ${this.name} started`);
        return true;
    }

    /**
     * @param {number} delta
     */
    onTick(delta) {
        this.kill();
    }

    kill() {
        this.node.processKilled(this.pid);
        this.onKill();
        this.killed = true;
    }

    onKill() {
        log(LogType.INFO, `Process ${this.name} killed`);
    }

    /**
     * @param {number} amt Amount of RAM the process now consumes
     */
    setRamCost(amt) {
        this.ramUsage = amt;
        this.node.onProcessUpdate(this, true);
    }
}
