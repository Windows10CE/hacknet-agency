import Process from './Process.js';

import ProcessPortHack from './ProcessPortHack.js';
import {ProcessSSHCrack, ProcessFTPBounce} from './ProcessCrackers.js';

const Programs = {
    default: Process,
    portHack: ProcessPortHack,
    sshCrack: ProcessSSHCrack,
    ftpBounce: ProcessFTPBounce
};

export default Programs;
