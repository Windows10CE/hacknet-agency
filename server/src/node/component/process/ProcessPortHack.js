/** @typedef {import('../../Node.js').default} Node */
/** @typedef {import('../../../Character.js').default} Character */

import Process from './Process.js';

import log, {LogType} from '../../../Logger.js';

export default class ProcessPortHack extends Process {
    crackDuration = 10;

    /** @type {Node} */
    targetNode = null;

    targetPort = 0;

    execName = '';
    
    /**
     * @param {number} pid
     * @param {string} name
     * @param {Node} node
     * @param {Character} character
     */
    constructor(pid, name, node, character) {
        super(pid, name, node, character);
        this.targetNode = character.currentNode;
    }

    /**
     * @param {string[]} args
     */
    onStart(args) {
        this.execName = args[0];
        if (!this.character.currentNode.isVulnerable()) {
            this.character.writeToTerminal(`${args[0]}: Node ${this.character.currentNode.address} isn't vulnerable`);
            this.kill();
            return false;
        }
        return true;
    }

    /**
     * @param {number} delta
     */
    onTick(delta) {
        if (this.character.currentNode !== this.targetNode) {
            this.character.writeToTerminal(`Privilege escalation on ${this.targetNode.address} cancelled by user`);
            this.kill();
            return;
        }
        this.crackDuration -= delta;
        if (this.crackDuration <= 0) {
            if (this.character.currentNode.isVulnerable()) {
                this.character.currentNode.grantAdmin(this.character);
            }
            else {
                this.character.writeToTerminal(`Privilege escalation on ${this.targetNode.address} failed`);
            }
            this.kill();
            return;
        }
    }

    onKill() {

    }
}
