/** @typedef {import('../../Node.js').default} Node */
/** @typedef {import('../../../Character.js').default} Character */

import ProcessPortCracker from './ProcessPortCracker.js';

import log, {LogType} from '../../../Logger.js';

export class ProcessFTPBounce extends ProcessPortCracker {
    targetProtocol = 'ftp';

    crackDuration = 10;
    /**
     * @param {number} pid
     * @param {string} name
     * @param {Node} node
     * @param {Character} character
     */
    constructor(pid, name, node, character) {
        super(pid, name, node, character);
    }
}

export class ProcessSSHCrack extends ProcessPortCracker {
    targetProtocol = 'ssh';

    crackDuration = 10;
    /**
     * @param {number} pid
     * @param {string} name
     * @param {Node} node
     * @param {Character} character
     */
    constructor(pid, name, node, character) {
        super(pid, name, node, character);
    }
}
