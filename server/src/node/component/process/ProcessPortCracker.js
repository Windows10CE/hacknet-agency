/** @typedef {import('../../Node.js').default} Node */
/** @typedef {import('../../../Character.js').default} Character */

import Process from './Process.js';

import log, {LogType} from '../../../Logger.js';

export default class ProcessPortCracker extends Process {
    targetProtocol = 'defaultPort';

    crackDuration = 10;

    /** @type {Node} */
    targetNode = null;

    targetPort = 0;
    
    /**
     * @param {number} pid
     * @param {string} name
     * @param {Node} node
     * @param {Character} character
     */
    constructor(pid, name, node, character) {
        super(pid, name, node, character);
        this.targetNode = character.currentNode;
    }

    /**
     * @param {string[]} args
     */
    onStart(args) {
        this.targetPort = parseInt(args[1]);
        if (isNaN(this.targetPort) || !isFinite(this.targetPort)) {
            this.character.writeToTerminal(`${args[0]}: Invalid port number provided`);
            this.kill();
            return false;
        }
        const port = this.targetNode.getPort(this.targetPort);
        if (!port || port.protocol !== this.targetProtocol) {
            this.character.writeToTerminal(`${args[0]}: Invalid port provided`);
            this.kill();
            return false;
        }
        if (port.open) {
            this.character.writeToTerminal(`${args[0]}: Port is already open`);
            this.kill();
            return false;
        }
        return true;
    }

    /**
     * @param {number} delta
     */
    onTick(delta) {
        this.crackDuration -= delta;
        if (this.character.currentNode !== this.targetNode) {
            this.character.writeToTerminal(`Port ${this.targetPort} cracking on ${this.targetNode.address} cancelled by user`);
            this.kill();
            return;
        }
        const port = this.targetNode.getPort(this.targetPort);
        if (this.crackDuration <= 0) {
            port.open = true;
            this.character.writeToTerminal(`Port ${this.targetPort} on ${this.targetNode.address} cracked`);
            this.kill();
            return;
        }
        if (port.open) {
            this.kill();
            return;
        }
    }

    onKill() {

    }
}
