/** @typedef {import('../../Character.js').default} Character */

export default class CharTracker {
    /** @type {Character} */
    character;

    /** @type {number} */
    currentAccount = -1;

    /** @type {boolean} */
    online = true;

    /**
     * @param {Character} character
     * @param {number} [accountId]
     */
    constructor(character, accountId) {
        this.character = character;
        if (accountId === undefined) {
            accountId = -1;
        }
        this.currentAccount = accountId;
    }
}
