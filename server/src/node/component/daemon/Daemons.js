import MailDaemon from './MailDaemon.js';
import MissionBoardDaemon from './MissionBoardDaemon.js';

const DaemonTypes = {
    'mail': MailDaemon,
    'mission-board': MissionBoardDaemon
};

export default DaemonTypes;
