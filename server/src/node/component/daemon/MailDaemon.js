import Daemon from './Daemon.js';

/** @typedef {import('../../Node.js').default} Node */
/** @typedef {import('../fs/Directory.js').default} Directory */
/** @typedef {import('../../../Character.js').default} Character */

import Mail from '../fs/special/Mail.js';

import PacketMailFetch from '../../../network/packet/game/PacketMailFetch.js';
import PacketMailRead from '../../../network/packet/game/PacketMailRead.js';


export default class MailDaemon extends Daemon {
    type = 'mail';

    /** @type {Directory} */
    mailFolder = null;

    /**
     * @param {Node} node
     */
    constructor(node) {
        super(node);
    }

    create() {
        this.node.getRoot().createDirectory('mail');
    }

    init() {
        this.mailFolder = /** @type {Directory} */ (this.node.getRoot().getChild('mail'));
    }

    /**
     * @param {string} accountName
     * @returns {Directory}
     */
    getUserDir(accountName) {
        if (!this.node.getAccount(accountName)) {
            return;
        }
        let userDir = /** @type {Directory} */ (this.mailFolder.getChild(accountName));
        if (!userDir) {
            userDir = this.mailFolder.createDirectory(accountName);
        }
        return userDir;
    }

    /**
     * @param {string} accountName
     * @param {*} mail
     */
    receiveMail(accountName, mail) {
        if (!accountName) {
            return;
        }
        const userDir = this.getUserDir(accountName);
        if (!userDir) {
            return;
        }
        const mailFile = new Mail(userDir);
        mailFile.author = mail.sender;
        mailFile.subject = mail.subject;
        mailFile.content = mail.body;
        mailFile.relatedMission = mail.relatedMission;
        mailFile.read = false;
        let mailId = 0;
        while (true) {
            if (!userDir.getChild(mailId.toString())) {
                mailFile.name = `mail${mailId}`;
                break;
            }
            mailId++;
        }
        userDir.addFile(mailFile);
    }

    /**
     * @param {Character} character
     * @param {string[]} request
     */
    onDaemonRequest(character, request) {
        const mailAccount = this.node.getCurrentAccount(character);
        if (mailAccount === null) {
            character.writeToTerminal('You do not have a mail account on this server.');
            return;
        }
        let userDir = this.getUserDir(mailAccount.name);
        if (!userDir) {
            this.mailFolder.createDirectory(mailAccount.name);
        }
        userDir = this.getUserDir(mailAccount.name);

        if (request.length >= 1) {
            if (request[0] === 'fetch-all') {
                const mails = [];
                for (const mail of userDir.children.filter((file) => file.type === 'mail')) {
                    mails.push(/** @type{Mail} */(mail).getMailHeader());
                }
                character.pilot.send(new PacketMailFetch(mails));
            }
            else if (request[0] === 'fetch') {
                const mails = [];
                for (const mail of userDir.children.filter((file) => file.type === 'mail' && /** @type {Mail} */(file).read === false)) {
                    mails.push(/** @type{Mail} */(mail).getMailHeader());
                }
                character.pilot.send(new PacketMailFetch(mails));
            }
            else if (request[0] === 'read') {
                const mail = userDir.getChild(request[1]);
                if (mail && mail.type === 'mail') {
                    character.pilot.send(new PacketMailRead(/** @type {Mail} */ (mail).getMail()));
                    /** @type {Mail} */ (mail).setRead();
                }
                else {
                    character.writeToTerminal('Requested mail isn\'t available.');
                }
            }
            else if (request[0] === 'reply') {
                const mail = userDir.getChild(request[1]);
                if (mail && mail.type === 'mail') {
                    if (/** @type{Mail} */(mail).reply(character, request[2])) {
                        /** @type{Mail} */(mail).relatedMission = undefined;
                    }
                }
            }
        }
        else {
            const unread = userDir.children.filter((file) => {
                return file.type === 'mail' && /** @type {Mail} */(file).read === false;
            }).length;
            character.writeToTerminal(`Mail account: ${mailAccount.name}. Unread mails: ${unread}`);
        }
    }

    /**
     * @param {Node} node
     * @param {*} data
     * @returns {Daemon}
     */
    static deserialize(node, data) {
        return new MailDaemon(node);
    }
}
