/** @typedef {import('../../Node.js').default} Node */
/** @typedef {import('../../../Character.js').default} Character */

export default class Daemon {
    type = 'none';

    /** @type {Node} */
    node = null;

    /**
     * @param {Node} node
     */
    constructor(node) {
        this.node = node;
    }

    create() {

    }

    init() {

    }

    /**
     * @param {Character} character
     * @param {string[]} request
     */
    onDaemonRequest(character, request) {

    }

    /**
     * @param {Node} node
     * @param {*} data
     * @returns {Daemon}
     */
    static deserialize(node, data) {
        throw 'Unimplemented';
    }
}
