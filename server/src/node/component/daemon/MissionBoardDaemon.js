import Daemon from './Daemon.js';

/** @typedef {import('../../Node.js').default} Node */
/** @typedef {import('../fs/Directory.js').default} Directory */
/** @typedef {import('../../../Character.js').default} Character */

/** @typedef {import('../fs/special/Mail.js').default} Mail */


export default class MissionBoardDaemon extends Daemon {
    type = 'mission-board';

    /**
     * @param {Node} node
     */
    constructor(node) {
        super(node);
    }

    create() {

    }

    init() {
        
    }

    /**
     * @param {Character} character
     * @param {string[]} request
     */
    onDaemonRequest(character, request) {
        
    }

    /**
     * @param {Node} node
     * @param {*} data
     * @returns {Daemon}
     */
    static deserialize(node, data) {
        return new MissionBoardDaemon(node);
    }
}
