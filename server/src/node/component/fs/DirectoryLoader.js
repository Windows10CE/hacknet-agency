import File, { Tokens } from './File.js';
import Directory from './Directory.js';

import Executable from './special/Executable.js';
import Mail from './special/Mail.js';

import log, { LogType } from './../../../Logger.js';

const FileTypes = {
    exe: Executable,
    mail: Mail
};

/**
 * @param {Directory} parent
 * @param {*} data
 * @returns {File[]}
 */
function walk(parent, data) {
    const result = [];

    for (const name in data) {
        const object = data[name];
        let file;
        if (isDirectory(object)) {
            file = new Directory(parent);
            file.children = walk(file, object);
        }
        else {
            if (typeof object === 'string') {
                // Raw data file
                file = new File(parent);
                file.content = object;
            }
            else if (Array.isArray(object)) {
                // Multi-line file
                file = new File(parent);
                file.content = object.join('\n');
            }
            else {
                const type = object['#type'];
                const typeClass = FileTypes[type];
                if (typeClass) {
                    file = typeClass.deserialize(parent, object);
                }
                else {
                    log(LogType.WARNING, `Unknown complex file type ${type}. Ignoring.`);
                    continue;
                }
            }
        }

        file.name = name;
        result.push(file);
    }

    return result;
}

function isDirectory(data) { return typeof data === 'object' && data.constructor.name !== 'Array' && !(Tokens.COMPLEX_FILE in data); }

/**
 * @param {any} data
 */
export default function(data) {
    const result = new Directory(null);
    result.name = '';
    result.children = walk(result, data);

    return result;
}
