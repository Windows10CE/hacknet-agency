import File from './File.js';

import { Tokens } from './File.js';

/**
 * @param {string} pattern
 * @returns {boolean} Whether the pattern is a glob pattern or not
 */
function isGlobPattern(pattern) {
    if (pattern.indexOf(Tokens.STAR_WILDCARD) > -1) {
        return true;
    }
    if (pattern.indexOf(Tokens.QUESTION_WILDCARD) > -1) {
        return true;
    }
    return false;
}

/**
 * @param {File} cur Root of the walk search
 * @param {string[]} paths String array representing the path to search
 * @param {boolean} [glob] Whether globbing is enabled for the search
 * @returns {File[]} Array of files matching the search
 */
function walkFind(cur, paths, glob = false) {
    if (!cur) {
        return [];
    }
    if (paths.length === 0) {
        return [cur];
    }
    else if (cur.type !== 'dir') {
        return [];
    }
    const curDir = /** @type {Directory} */ (cur);
    const direction = paths.shift();
    switch (direction) {
    case Tokens.THIS:
        return walkFind(curDir, paths, glob);
    case Tokens.UP:
        return walkFind(curDir.getSafeParent(), paths, glob);
    default:
        if (isGlobPattern(direction)) {
            if (!glob) {
                return [];
            }
            // Do globbing
            return [];
        }
        return walkFind(curDir.getChild(direction), paths, glob);
    }
}

export default class Directory extends File {
    /** @type {string} */
    type = 'dir';

    /** @type {File[]} */
    children = [];

    /**
     * @param {?Directory} parent
     */
    constructor(parent) {
        super(parent);
    }

    /**
     * @returns {Directory} Root directory of this directory
     */
    get root() {
        return this.parent ? this.parent.root : this;
    }

    /**
     * @param {string} name
     * @returns {File} Child File exactly named 'name'
     */
    getChild(name) {
        return this.children.find((file) => file.name === name);
    }

    /**
     * @param {string} pathStr Path of the search
     * @returns {File[]} File array
     */
    getFiles(pathStr) {
        let working = /** @type {Directory} */(this);
        if (pathStr === Tokens.DIRECTORY) {
            return [this.root];
        }
        if (pathStr[0] === Tokens.DIRECTORY) {
            working = this.root;
            pathStr = pathStr.slice(1);
        }
        const paths = pathStr.split(Tokens.DIRECTORY);
        
        return walkFind(working, paths, true);
    }

    /**
     * @param {string} pathStr Path of the search
     * @returns {File} File array
     */
    getFile(pathStr) {
        let working = /** @type {Directory} */(this);
        if (pathStr === Tokens.DIRECTORY) {
            return this.root;
        }
        if (pathStr[0] === Tokens.DIRECTORY) {
            working = this.root;
            pathStr = pathStr.slice(1);
        }
        const paths = pathStr.split(Tokens.DIRECTORY);

        const walk = walkFind(working, paths, false);
        return walk.length > 0 ? walk[0] : null;
    }
    
    /**
     * @returns {!Directory} Returns parent of the file; 'this' if there's no parent.
     */
    getSafeParent() { return !this.parent ? this : this.parent; }

    /**
     * Creates and add file to the directory
     * @param {string} name Name of the file to create
     * @param {string} [content] Content of the file
     * @returns {File}
     */
    createFile(name, content = '') {
        const file = new File(this);
        file.name = name;
        file.content = content;
        this.addFile(file);
        return file;
    }

    /**
     * Creates and add directory to the current directory
     * @param {string} name Name of the file to create
     * @returns {Directory}
     */
    createDirectory(name) {
        const file = new Directory(this);
        file.name = name;
        this.addFile(file);
        return file;
    }

    /**
     * Adds the given file to the directory
     * @param {File} file
     */
    addFile(file) {
        if (this.children.find((f) => f === file)) {
            return;
        }
        this.children.push(file);
        file.parent = this;
    }

    /**
     * Remove the given file from the directory
     * @param {File} file
     */
    deleteFile(file) {
        this.children = this.children.filter((f) => f !== file);
        if (file.parent === this) {
            file.parent = null;
        }
    }

    /**
     * Deletes the current Directory from the parent, if possible
     * @returns {number} Error code (2: root, 1: not empty, 0: ok)
     */
    delete() {
        if (this.root === this) {
            return 2;
        }
        if (this.children.length > 0) {
            return 1;
        }
        this.parent.deleteFile(this);
        return 0;
    }

    /**
     * Gets an object representation of this instance
     * @returns {*} Object representation
    */
    serialize() {
        const result = {};
        for (const child of this.children) {
            result[child.name] = child.serialize();
        }
        return result;
    }
}
