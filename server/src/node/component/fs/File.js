/** @typedef {import('./Directory.js').default} Directory */

export const Tokens = {
    DIRECTORY: '/',
    HOME: '~',
    THIS: '.',
    UP: '..',
    COMPLEX_FILE: '#type',
    STAR_WILDCARD: '*',
    QUESTION_WILDCARD: '?'
};

export default class File {
    /** @type {string} */
    type = 'file';

    /** @type {string} */
    name = '';

    /** @type {string} */
    content = '';

    /** @type {Directory} */
    parent = null;

    /**
     * @param {?Directory} parent
     */
    constructor(parent) {
        this.parent = parent;
    }

    /**
     * @returns {Directory[]} Parent Directory List
     */
    getParents() {
        if (this.parent === null) {
            return [];
        }
        return [this.parent].concat(this.parent.getParents());
    }

    /**
     * @returns {string} Path to the file from root
     */
    getAbsolutePath() {
        return Tokens.DIRECTORY + this.getParents().map(file => file.name).join(Tokens.DIRECTORY) + this.name;
    }

    /**
     * Deletes the current File from the parent
     * @returns {number} 0: ok
     */
    delete() {
        this.parent.deleteFile(this);
        return 0;
    }

    /**
     * Gets an object representation of this instance
     * @returns {*} Object representation
    */
    serialize() {
        let result;
        if (this.content.indexOf('\n') > -1) {
            // Multi-line file
            result = this.content.split('\n');
        }
        else {
            // Raw data file
            result = this.content;
        }
        return result;
    }

    /**
     * @param {Directory} parent
     * @param {*} data
     * @returns {File}
     */
    static deserialize(parent, data) {
        return new File(parent);
    }
}
