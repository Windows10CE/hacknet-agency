import File from '../File.js';

/** @typedef {import('../Directory.js').default} Directory */

/** @typedef {import('../../../../Character.js').default} Character */
/** @typedef {import('../../../../mission/Mission.js').default} Mission */

export default class Mail extends File {
    type = 'mail';

    name = '';

    subject = '';

    author = '';

    content = '';

    read = false;

    /** @type {Mission} */
    relatedMission;

    constructor(parent) {
        super(parent);
    }

    getMailHeader() {
        return {name:this.name, subject:this.subject, author:this.author, read:this.read};
    }

    getMail() {
        return {name:this.name, subject:this.subject, author:this.author, content:this.content, canReply: this.relatedMission !== undefined};
    }

    setRead() {
        this.read = true;
    }

    /**
     * @param {Character} character
     * @param {*} data
     * @returns {boolean}
     */
    reply(character, data) {
        if (this.relatedMission) {
            return this.relatedMission.check(character, data);
        }
        return false;
    }

    /**
     * Gets an object representation of this instance
     * @returns {*} Object representation
    */
    serialize() {
        const result = {};
        result['#type'] = this.type;
        result.subject = this.subject;
        result.author = this.author;
        result.content = this.content;
        if (this.read) {
            result.read = this.read;
        }
        return result;
    }

    /**
     * @param {Directory} parent
     * @param {*} data
     * @returns {Mail}
     */
    static deserialize(parent, data) {
        const result = new Mail(parent);
        result.subject = data.subject;
        result.author = data.author;
        result.content = data.content;
        result.read = 'read' in data ? data.read : false;
        // TODO attachments
        return result;
    }
}
