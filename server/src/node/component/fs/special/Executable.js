import File from '../File.js';

/** @typedef {import('../Directory.js').default} Directory */

/** @typedef {import('../../../../Character.js').default} Character */
/** @typedef {import('../../../../mission/Mission.js').default} Mission */

export default class Executable extends File {
    type = 'exe';

    target = '';

    name = '';

    /**
     * @param {Directory} parent
     */
    constructor(parent) {
        super(parent);
    }

    /**
     * Gets an object representation of this instance
     * @returns {*} Object representation
    */
    serialize() {
        const result = {};
        result['#type'] = this.type;
        result.exe = this.target;
        return result;
    }

    /**
     * @param {Directory} parent
     * @param {*} data
     * @returns {Executable}
     */
    static deserialize(parent, data) {
        const result = new Executable(parent);
        result.target = data.exe;
        return result;
    }
}
