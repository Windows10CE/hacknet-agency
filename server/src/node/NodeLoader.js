import Node from './Node.js';
/** @typedef {import('../World.js').default} World */
import log, {LogType} from '../Logger.js';
import {Tokens} from './component/fs/File.js';
import DirectoryLoader from './component/fs/DirectoryLoader.js';
import Daemons from './component/daemon/Daemons.js';

import { generatePassword } from '../misc/Util.js';

/**
 * @param {World} world
 * @param {any} data
 * @returns {Node}
 */
export default function(world, data) {
    const result = new Node(world);
    result.id = data.id;
    result.displayName = data.display_name;

    if (data.ports !== undefined && data.ports !== undefined) {
        for (const port of data.ports) {
            result.ports.push({
                num: port.num,
                protocol: port.protocol,
                open: false
            });
        }
    }
    result.portsToCrack = data.ports_to_crack ? data.ports_to_crack : 0;

    /* Optional elements */
    result.proxy = data.proxy;
    result.address = data.address;
    result.firewall = data.firewall;
    result.monitor = data.monitor;

    if (Array.isArray(data.accounts)) {
        for (const accountData of data.accounts) {
            const account = {
                name: accountData.name,
                password: null,
                admin: 'admin' in accountData ? accountData.admin : false,
                home: Tokens.DIRECTORY + 'home'
            };
            if (account.admin) {
                account.home = Tokens.DIRECTORY;
            }
            if ('password' in accountData && accountData.password !== null) {
                if (typeof accountData.password === 'string') {
                    account.password = accountData.password;
                }
                else if (typeof accountData.password === 'number') {
                    account.password = generatePassword(accountData.password);
                }
                else {
                    log(LogType.WARNING, `Invalid password field type for account ${account.name} of node ${result.id}. Generating instead random passwd.`);
                    account.password = generatePassword(null);
                }
            }
            else {
                account.password = null;
            }
            result.accounts.push(account);
        }
    }

    // Load Daemons
    if (data.daemons) {
        for (const daemonData of data.daemons) {
            const daemonType = Daemons[daemonData.type];
            if (daemonType === undefined) {
                log(LogType.WARNING, `Invalid daemon type ${daemonData.type} in node ${result.id}. This daemon won't be created.`);
            }
            result.daemons.push(daemonType.deserialize(result, daemonData));
        }
    }

    result.root = DirectoryLoader(data.filesystem);
    result.dependencies.links = data.links ? data.links : [];

    log(LogType.VERBOSE, `Loaded node ${result.id}`);

    return result;
}
