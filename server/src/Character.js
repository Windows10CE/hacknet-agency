/** @typedef {import('./World.js').default} World */
/** @typedef {import('./node/Node.js').default} Node */

/** @typedef {import('./Client.js').default} Client */
/** @typedef {import('./node/component/fs/Directory.js').default} Directory */
/** @typedef {import('./node/component/daemon/MailDaemon.js').default} MailDaemon */


import log, {LogType} from './Logger.js';

import PacketTerminalMessage from './network/packet/game/PacketTerminalMessage.js';

import { generatePassword } from './misc/Util.js';

export default class Character {
    /** @type {*} */
    dependencies = {
        homeNode: null,
        mailServer: null
    };

    //=====| CHARACTER INFO |=====\\
    /** @type {string} */
    name;

    /** @type {Node} */
    homeNode;

    /** @type {MailDaemon} */
    mailServer;

    //=====| TECHNICAL DATA |=====\\
    /** @type {World} */
    world;
    
    id = -1;
    /** @type {Client} */
    pilot = null;

    //=====| GAME DATA |=====\\
    /** @type {Node} */
    _currentNode = null;

    /** @type {{node: Node, acctName: string, acctPassword: string}[]} */
    knownAccounts = [];

    /**
     * @returns {Node} Current connected node
     */
    get currentNode() {
        return this._currentNode ? this._currentNode : this.homeNode;
    }

    set currentNode(val) {
        this._currentNode = val;
    }

    /** @type {Directory} */
    _currentDirectory = null;

    get currentDirectory() {
        return this._currentDirectory ? this._currentDirectory : this.currentNode.root;
    }

    set currentDirectory(val) {
        this._currentDirectory = val;
    }
    

    /**
     * @param {World} world
     * @param {number} id
     */
    constructor(world, id) {
        this.world = world;
        this.id = id;
    }

    /**
     * @param {World} world
     * @returns {boolean} Whether the character is valid in its world
     */
    resolve(world) {
        // Handle home node
        this.homeNode = world.getNodeFromId(this.dependencies.homeNode);
        if (!this.homeNode) {
            log(LogType.ERROR, `Failed to resolve home node ${this.dependencies.homeNode} for character ${this.name}`);
            return false;
        }
        this.homeNode.owner = this;

        // Handle mail server
        const mailNode = world.getNodeFromId(this.dependencies.mailServer);
        if (!mailNode) {
            log(LogType.ERROR, `Failed to resolve mail server node ${this.dependencies.mailServer} for character ${this.name}`);
            return false;
        }
        this.mailServer = /** @type {MailDaemon} */ (mailNode.daemons.find((daemon) => daemon.type === 'mail'));
        if (!this.mailServer) {
            log(LogType.ERROR, `Failed to resolve mail server daemon of ${this.dependencies.mailServer} for character ${this.name}`);
            return false;
        }
        let account = mailNode.accounts.find((account) => account.name === this.name);
        if (!account) {
            account = {name: this.name, password: generatePassword(), home: 'mail/' + this.name, admin: false};
            mailNode.accounts.push(account);
        }
        this.knownAccounts.push({node: mailNode, acctName: this.name, acctPassword: account.password});

        return true;
    }

    /**
     * @returns {World}
     */
    getWorld() {
        return this.world;
    }

    /**
     * Called when a pilot Client is linked to the Character
     * @param {Client} pilot
     */
    spawn(pilot) {
        this.pilot = pilot;
        this.currentNode.onConnect(this);
    }

    despawn() {
        this.pilot = null;
        this.disconnect();
    }

    /**
     * @param {string} message Message to write on the character's terminal
     */
    writeToTerminal(message, end = '\n') {
        const pack = new PacketTerminalMessage(message + end);
        if (this.pilot) {
            this.pilot.send(pack);
        }
    }

    sendToMailServer(mail) {
        this.mailServer.receiveMail(this.name, mail);
    }

    getPilot() {
        return this.pilot;
    }

    getCharacterInfo() {
        return {id: this.id, name: this.name};
    }

    disconnect() {
        this.currentNode.onDisconnect(this);
        this.currentNode = null;
        this.currentDirectory = null;
    }

    /**
     * @param {Node} target
     */
    connect(target) {
        /** Send @packet to notify connection */
        this.currentNode = target;
        this.currentDirectory = target.root;
        target.onConnect(this);
    }

    /**
     * @param {Directory} targetDir
     */
    changeDir(targetDir) {
        this.currentDirectory = targetDir;
    }
}
