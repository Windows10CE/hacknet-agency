import Mission from './Mission.js';
import Actions from './action/Actions.js';
import Goals from './goal/Goals.js';
import Path from './Path.js';

import log, {LogType} from '../Logger.js';

/** @typedef {import('../World.js').default} World */
/** @typedef {import('./action/Action.js').default} Action */
/** @typedef {import('./goal/Goal.js').default} Goal */

/**
 * @param {*} object
 * @returns {Action}
 */
function loadAction(object) {
    const actionType = Actions[object.type];
    if (actionType === undefined) {
        throw `Action type ${object.type} is invalid`;
    }
    return actionType.deserialize(object);
}

/**
 * @param {*} object
 * @returns {Goal}
 */
function loadGoal(object) {
    const goalType = Goals[object.type];
    if (goalType === undefined) {
        throw `Goal type ${object.type} is invalid`;
    }
    const result = goalType.deserialize(object);
    return result;
}

/**
 * @param {*} object
 * @returns {Path}
 */
function loadPath(object) {
    const r = new Path();
    for (const data of object.goals) {
        r.goals.push(loadGoal(data));
    }
    for (const data of object.actions) {
        r.actions.push(loadAction(data));
    }
    return r;
}

/**
 * @param {any} data
 * @returns {Mission}
 */
export default function(data) {
    const result = new Mission();

    result.id = data.id;
    result.auto = data.auto;
    for (const object of data.paths) {
        result.paths.push(loadPath(object));
    }
    for (const object of data.start_actions) {
        result.startActions.push(loadAction(object));
    }
    for (const object of data.end_actions) {
        result.endActions.push(loadAction(object));
    }

    log(LogType.VERBOSE, `Loaded mission ${result.id}`);

    return result;
}
