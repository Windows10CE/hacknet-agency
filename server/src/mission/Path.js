import log, { LogType } from '../Logger.js';

/** @typedef {import('./goal/Goal.js').default} Goal */
/** @typedef {import('./action/Action.js').default} Action */
/** @typedef {import('../World.js').default} World */
/** @typedef {import('../Character.js').default} Character */

export default class Path {
    /** @type {Goal[]} */
    goals = [];

    /** @type {Action[]} */
    actions = [];

    /**
     * @param {Character} character
     * @param {*} data
     * @returns {boolean}
     */
    check(character, data) {
        for (const goal of this.goals) {
            if (!goal.check(character.world, character)) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param {Character} character
     * @param {World} world
     */
    execute(character, world) {
        for (const action of this.actions) {
            action.execute(character, world, null);
        }
    }

    /**
     * @param {World} world
     * @returns {boolean}
     */
    resolve(world) {
        for (const goal of this.goals) {
            if (!goal.resolve(world)) {
                log(LogType.ERROR, `Resolving path goal ${goal.type} failed.`);
                return false;
            }
        }

        for (const action of this.actions) {
            if (!action.resolve(world)) {
                log(LogType.ERROR, `Resolving path end action ${action.type} failed.`);
                return false;
            }
        }

        return true;
    }
}
