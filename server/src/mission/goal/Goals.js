import GoalDelete from './GoalDelete.js';
import GoalDownload from './GoalDownload.js';
import GoalUpload from './GoalUpload.js';
import GoalGainAccess from './GoalGainAccess.js';
import GoalReply from './GoalReply.js';

const GoalTypes = {
    'delete': GoalDelete,
    'download': GoalDownload,
    'upload': GoalUpload,
    'gainaccess': GoalGainAccess,
    'reply': GoalReply
};

export default GoalTypes;
