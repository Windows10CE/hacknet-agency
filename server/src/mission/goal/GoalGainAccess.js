/** @typedef {import('../../World.js').default} World */
/** @typedef {import('../../Character').default} Character */
/** @typedef {import('../../node/Node.js').default} Node */
import GoalNode from './GoalNode.js';

export default class GoalGainAccess extends GoalNode {
    type = 'gainaccess';

    /**
     * @param {World} world
     * @param {Character} character
     * @returns {boolean}
     */
    check(world, character) {
        for (const acc of this.target.accounts) {
            // Can't check if the player has admin access
        }
        return false;
    }
}
