/** @typedef {import('../../World.js').default} World */
/** @typedef {import('../../Character').default} Character */
/** @typedef {import('../../node/Node.js').default} Node */
import Goal from './Goal.js';

export default class GoalReply extends Goal {
    type='reply';
    
    msg = '';

    /**
     * @param {World} world
     * @param {Character} character
     * @returns {boolean}
     */
    check(world, character) {
        // Can't check email to see if there is any emails
    }

    /**
     * @virtual
     * @param {any} object
     * @returns {GoalReply}
     */
    static deserialize(object) {
        const r = new GoalReply();
        r.msg = object.message;
        return r;
    }
}
