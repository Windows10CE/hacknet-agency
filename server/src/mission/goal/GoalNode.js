/** @typedef {import('../../World.js').default} World */
/** @typedef {import('../../node/Node.js').default} Node */
import log, {LogType} from '../../Logger.js';
import Goal from './Goal.js';

export default class GoalNode extends Goal {
    dependencies = {
        target: ''
    };

    /** @type {Node} */
    target;

    constructor() {
        super();
    }

    /**
     * @virtual
     *
     * @param {World} world
     * @returns {boolean}
     */
    resolve(world) {
        this.target = world.getNodeFromId(this.dependencies.target);
        if (!this.target) {
            log(LogType.ERROR, `Failed to resolve target ${this.dependencies.target}`);
            return false;
        }

        return true;
    }

    /**
     * @virtual
     * @param {any} object
     * @returns {GoalNode}
     */
    static deserialize(object) {
        const r = new GoalNode();
        r.dependencies.target = object.target;
        return r;
    }
}
