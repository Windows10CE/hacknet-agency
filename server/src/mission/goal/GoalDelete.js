/** @typedef {import('../../World.js').default} World */
/** @typedef {import('../../Character.js').default} Character */
/** @typedef {import('../../node/Node.js').default} Node */
import GoalNode from './GoalNode.js';

export default class GoalDelete extends GoalNode {
    type = 'delete';

    filepaths = [];
    
    /**
     * @param {World} world
     * @param {Character} character
     * @returns {boolean}
     */
    check(world, character) {
        // Check for file deletion (or the file was moved/renamed)
        for (const file of this.filepaths) {
            if (this.target.root.getFiles(file).length !== 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * @virtual
     * @param {any} object
     * @returns {GoalDelete}
     */
    static deserialize(object) {
        const r = new GoalDelete();
        r.dependencies.target = object.target;
        r.filepaths = object.files;
        return r;
    }
}
