/** @typedef {import('./goal/Goal.js').default} Goal */
/** @typedef {import('./action/Action.js').default} Action */
/** @typedef {import('./Path.js').default} Path */
/** @typedef {import('../node/Node.js').default} Node */
/** @typedef {import('../World.js').default} World */
/** @typedef {import('../Character.js').default} Character[] */
import log, {LogType} from '../Logger.js';

export default class Mission {
    /** @type {string} */
    id;

    /** @type {World} */
    world;

    /** @type {boolean} */
    auto = false;

    /** @type {Path[]} */
    paths = [];

    /** @type {Action[]} */
    startActions = [];

    /** @type {Action[]} */
    endActions = [];

    constructor() {

    }

    /**
     * @param {Character} character
     */
    launch(character) {
        for (const action of this.startActions) {
            action.execute(character, this.world, this);
        }
    }

    /**
     * @param {Character} character
     * @param {*} data
     * @returns {boolean}
     */
    check(character, data) {
        for (const path of this.paths) {
            if (path.check(character, data)) {
                path.execute(character, character.world);
                character.world.missionComplete(character, this);
                return true;
            }
        }
        return false;
    }

    /**
     * @param {World} world
     * @returns {boolean}
     */
    resolve(world) {
        this.world = world;
        for (const path of this.paths) {
            if (!path.resolve(world)) {
                log(LogType.ERROR, `Resolving mission ${this.id} paths failed.`);
                return false;
            }
        }

        for (const action of this.startActions) {
            if (!action.resolve(world)) {
                log(LogType.ERROR, `Resolving mission ${this.id} start action ${action.type} failed.`);
                return false;
            }
        }

        for (const action of this.endActions) {
            if (!action.resolve(world)) {
                log(LogType.ERROR, `Resolving mission ${this.id} end action ${action.type} failed.`);
                return false;
            }
        }

        return true;
    }
}
