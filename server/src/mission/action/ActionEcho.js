import Action from './Action.js';
/** @typedef {import('../../World.js').default} World */
/** @typedef {import('../../Character.js').default} Character */
/** @typedef {import('../Mission.js').default} Mission */

import log, {LogType} from '../../Logger.js';

export default class ActionEcho extends Action {
    /** @type {string} */
    type = 'echo';

    /** @type {string} */
    msg;

    constructor() {
        super();
    }

    /**
     * @param {Character} character
     * @param {World} world
     * @param {Mission} context
     */
    execute(character, world, context) {
        log(LogType.INFO, `Mission Echo: ${this.msg}`);
    }

    /**
     * @virtual
     * @param {World} world
     * @returns {boolean}
     */
    resolve(world) { return true; }

    /**
     * @virtual
     * @param {any} object
     * @returns {ActionEcho}
     */
    static deserialize(object) {
        const r = new ActionEcho();
        r.msg = object.msg;
        return r;
    }
}
