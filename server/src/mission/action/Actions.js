import ActionEcho from './ActionEcho.js';
import ActionMail from './ActionMail.js';
import ActionMailQuery from './ActionMailQuery.js';

const ActionTypes = {
    'echo': ActionEcho,
    'mail': ActionMail,
    'mail-query': ActionMailQuery
};

export default ActionTypes;
