/** @typedef {import('../../World.js').default} World */
/** @typedef {import('../../Character.js').default} Character */
/** @typedef {import('../Mission.js').default} Mission */

export default class Action {
    /** @type {string} */
    type;

    /**
     * @param {Character} character
     * @param {World} world
     * @param {Mission} context
     */
    execute(character, world, context) {}

    /**
     * @virtual
     * @param {World} world
     * @returns {boolean}
     */
    resolve(world) { return true; }

    /**
     * @virtual
     * @param {any} object
     * @returns {Action}
     */
    static deserialize(object) {
        return new Action();
    }
}
