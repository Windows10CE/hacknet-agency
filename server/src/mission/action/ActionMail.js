import Action from './Action.js';
/** @typedef {import('../../World.js').default} World */
/** @typedef {import('../../Character.js').default} Character */
/** @typedef {import('../Mission.js').default} Mission */

import log, {LogType} from '../../Logger.js';

export default class ActionMail extends Action {
    /** @type {string} */
    type = 'mail';

    /** @type {*} */
    msg = {
        sender: '',
        subject: '',
        body: '',
        attachments: []
    }

    constructor() {
        super();
    }

    /**
     * @param {Character} character
     * @param {World} world
     * @param {Mission} context
     */
    execute(character, world, context) {
        const msg = this.msg;
        character.sendToMailServer(this.msg);
    }

    /**
     * @virtual
     * @param {World} world
     * @returns {boolean}
     */
    resolve(world) { return true; }

    /**
     * @virtual
     * @param {any} object
     * @returns {ActionMail}
     */
    static deserialize(object) {
        const r = new ActionMail();
        r.msg = object.msg;
        if (Array.isArray(r.msg.body)) {
            r.msg.body = r.msg.body.join('\n');
        }
        return r;
    }
}
