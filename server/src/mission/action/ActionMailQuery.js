import Action from './Action.js';
/** @typedef {import('../../World.js').default} World */
/** @typedef {import('../../Character.js').default} Character */
/** @typedef {import('../Mission.js').default} Mission */

import log, {LogType} from '../../Logger.js';

export default class ActionMailQuery extends Action {
    /** @type {string} */
    type = 'mail-query';

    /** @type {*} */
    msg = {
        sender: '',
        subject: '',
        body: '',
        attachments: [],
    }

    constructor() {
        super();
    }

    /**
     * @param {Character} character
     * @param {World} world
     * @param {Mission} context
     */
    execute(character, world, context) {
        const msg = this.msg;
        if (!context) {
            log(LogType.WARNING, 'Action mail-query was executed outside the scope of a mission.');
        }
        else {
            msg.relatedMission = context;
        }
        character.sendToMailServer(this.msg);
    }

    /**
     * @virtual
     * @param {World} world
     * @returns {boolean}
     */
    resolve(world) { return true; }

    /**
     * @virtual
     * @param {any} object
     * @returns {ActionMailQuery}
     */
    static deserialize(object) {
        const r = new ActionMailQuery();
        r.msg = object.msg;
        if (Array.isArray(r.msg.body)) {
            r.msg.body = r.msg.body.join('\n');
        }
        return r;
    }
}
