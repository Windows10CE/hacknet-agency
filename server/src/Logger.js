import util from 'util';

export const LogType = {
    DEBUG: 0,
    VERBOSE: 1,
    INFO: 2,
    WARNING: 3,
    ERROR: 4,
    CRITICAL: 5,
    OOPS: 6
};

export const ESCAPE_SEQUENCE = String.fromCharCode(27);

const logSeverity = [
    {name: 'DEBG', background: 8, prefixColor: 7, foreground: 8}, // todo
    {name: 'VERB', background: 4, prefixColor: 7, foreground: 7},
    {name: 'INFO', background: 2, prefixColor: 0, foreground: 7},
    {name: 'WARN', background: 3, prefixColor: 0, foreground: 3},
    {name: 'ERR ', background: 9, prefixColor: 7, foreground: 9},
    {name: 'CRIT', background: 1, prefixColor: 7, foreground: 1}, // todo
    {name: 'OOPS', background: 0, prefixColor: 15, foreground: 0}
];

let logLevel = 0;

/**
 * @param {number} level
 */
export function setLogLevel(level) {
    logLevel = level;
}

function getPrefix(level) {
    const bg = level.background;
    const fg = level.prefixColor;
    const e = ESCAPE_SEQUENCE;
    return `${e}[48;5;${bg};38;5;${fg}m ${level.name} ${e}[49;38;5;${level.foreground}m`;
}

/**
 * @param {number} severity
 * @param {any} msg
 * @param {any} [inspect]
 */
export default function log(severity, msg, inspect) {
    if (severity < logLevel) {
        return;
    }
    if (!(severity in logSeverity)) {
        severity = LogType.OOPS;
        log(LogType.OOPS, `Invalid Log Type, provided ${severity}`);
        console.trace();
        severity = LogType.OOPS;
    }
    process.stdout.write(`${getPrefix(logSeverity[severity])} `);
    if (inspect) {
        process.stdout.write(util.inspect(msg, false, null, true));
    }
    else {
        process.stdout.write(msg);
    }
    console.log(`${ESCAPE_SEQUENCE}[0m`);
}

/**
 * @param {Object[]} entries
 * @param {number} [maxWidth]
 * @param {string} [separator]
 * @param {string} [padding]
 * @returns {string} The entries in string form
 */
export function table(entries, maxWidth, separator, padding) {
    if (!separator) {
        separator = ' ';
    }
    if (!padding) {
        padding = ' ';
    }

    const lines = [];

    // Work out widths
    const widths = [];
    for (const row of entries) {
        for (let i = 0; i < row.length; i++) {
            const curWidth = widths[i];
            if (maxWidth && curWidth >= maxWidth) {
                continue;
            }
            const width = row[i].length;
            if (!curWidth || width > curWidth) {
                widths[i] = maxWidth ? Math.min(maxWidth, width) : width;
            }
        }
    }

    // Create lines
    for (const row of entries) {
        const columns = [];
        for (let i = 0; i < row.length; i++) {
            const colWidth = widths[i];
            let content = row[i].toString().substring(0, colWidth);
            const width = content.length;

            if (width < colWidth) {
                let align = 0;
                // Check alignment
                if (typeof row[i] === 'number') {
                    align = 1;
                }

                // Pad content
                content = align === 0
                    ? content + padding.repeat(colWidth - width)
                    : padding.repeat(colWidth - width) + content;
            }

            columns.push(content);
        }
        lines.push(columns.join(separator));
    }

    return lines.join('\n');
}
