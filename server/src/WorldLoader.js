import process from 'process';
import fs from 'fs';
import path from 'path';

// @ts-ignore
const DIRNAME = process.cwd();

import World from './World.js';
import CharacterLoader from './CharacterLoader.js';
import log, {LogType} from './Logger.js';
import NodeLoader from './node/NodeLoader.js';
import MissionLoader from './mission/MissionLoader.js';


/**
* @param {string} dir
* @param {RegExp} filter
*/
function findInDir(dir, filter = /\.json$/, fileList = []) {
    const files = fs.readdirSync(dir);

    files.forEach((file) => {
        const filePath = path.join(dir, file);
        const fileStat = fs.lstatSync(filePath);

        if (fileStat.isDirectory()) {
            findInDir(filePath, filter, fileList);
        }
        else if (filter.test(filePath)) {
            fileList.push(filePath);
        }
    });

    return fileList;
}

/**
* @param {World} world
* @param {any} data
* @returns {boolean}
*/
function loadCharacters(world, data) {
    world.defaultCharacter = CharacterLoader(world, data[0], 0);
    for (let i = 1; i <= world.info.characterAmount.max; i++) {
        const pl = data[i];

        if ((pl === null || pl === undefined) && !world.defaultCharacter) {
            log(LogType.ERROR, `Character ${i} wasn't defined yet no default character was provided`);
            return false;
        }
        const character = CharacterLoader(world, pl, i);
        if (character === null) {
            log(LogType.ERROR, `Unable to create character ${i}`);
            return false;
        }
        world.characters.push(character);
    }
    return true;
}

/**
* @param {World} world
* @param {string} path
* @returns {boolean}
*/
function loadNodes(world, path) {
    const nodeFiles = findInDir(path);
    for (const file of nodeFiles) {
        try {
            const fd = fs.openSync(file, 'r');
            const data = JSON.parse(fs.readFileSync(fd).toString());
            fs.closeSync(fd);
            const node = NodeLoader(world, data);
            if (node !== null) {
                world.nodes.push(node);
            }
            else {
                throw 'Node failed to load.';
            }
        }
        catch (exception) {
            log(LogType.ERROR, `Unable to load node ${file}: ${exception}`);
            return false;
        }
    }
    return true;
}

/**
* @param {World} world
* @param {string} path
* @returns {boolean}
*/
function loadMissions(world, path) {
    const missionFiles = findInDir(path);
    for (const file of missionFiles) {
        try {
            const fd = fs.openSync(file, 'r');
            const data = JSON.parse(fs.readFileSync(fd).toString());
            fs.closeSync(fd);
            const mission = MissionLoader(data);
            if (mission !== null) {
                world.missions.push(mission);
            }
            // maybe just gracefully ignore the mission if it's null?
            // if so, apply this behaviour everywhere
            /*else {
                throw 'Mission failed to load.';
            }*/
        }
        catch (exception) {
            log(LogType.ERROR, `Unable to load mission ${file}: ${exception}`);
            return false;
        }
    }
    return true;
}

/**
* @param {string} path
* @returns {World}
*/
export default function(path) {
    let extData;
    try {
        const extInfoFile = fs.openSync(DIRNAME + '/' + path + '/extension_info.json', 'r');
        extData = JSON.parse(fs.readFileSync(extInfoFile).toString());
        fs.closeSync(extInfoFile);
        log(LogType.VERBOSE, 'Extension data read');
    }
    catch (exception) {
        log(LogType.ERROR, `Impossible to read extension_info.json in path ${path}: ${exception}`);
        return null;
    }
    const world = new World();

    world.info.name = extData.name;
    world.info.characterAmount.min = extData.min_players;
    world.info.characterAmount.max = extData.max_players;

    if (!loadCharacters(world, extData.characters)) {
        log(LogType.ERROR, 'Impossible to load characters.');
        return null;
    }
    if (!loadNodes(world, `${DIRNAME}/${path}/${extData.nodeFolder}`)) {
        log(LogType.ERROR, 'Impossible to load nodes.');
        return null;
    }
    if (!loadMissions(world, `${DIRNAME}/${path}/${extData.missionFolder}`)) {
        log(LogType.ERROR, 'Impossible to load missions.');
        return null;
    }

    for (const startMission of extData.starting_missions) {
        world.dependencies.startingMissions.push(startMission);
    }

    if (!world.resolve()) {
        log(LogType.ERROR, 'Resolving the world failed.');
        return null;
    }

    return world;
}
